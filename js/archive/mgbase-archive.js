// Mgbase

(function() {
	
	// Initial Setup
	
	// Save a reference to the global object
	var root = this;
	
	// The top-level namespace
	var Mgbase = root.Mgbase = {};
	
	// jQuery owns the '$' variable
	var $ = root.jQuery;
	
	// While jQuery is the default library, pass optional library in to override
	Mgbase.setDomLibrary = function(lib) {
		
		$ = lib;
		
	};
	
	// --------EVENTS---------
	// Custom event handling for *any object*
	var Events = Mgbase.Events = {
		
		on: function(events, callback, context) {
			
		},
		
		off: function(events, callback, context) {
			
		},
		
		trigger: function(events) {
			
		}
		
	};
	
	//Aliases for use of bind/unbind rather than on/off
	Events.bind = Events.on;
	
	Events.unbind = Events.off;
	
	
	// --------BASE CLASS---------
	var Class = Mgbase.Class = function(properties, options) {
		
		this.properties = _.extend({
			className: 'Class'
		}, properties);
		
		this._options = _.extend({
			autoSetReady: false
		}, options); 
		
		this._globals = {};
		
		this._escapedProperties = {};
		
		//Core settings
		this.cid = _.uniqueId(this.properties.className + '_');
		this.alive = false;
		this.ready = false;
		
		this.changed = {};
		this._silent = {};
	    this._pending = {};
		this.set(properties, {silent: true});
		//reset change tracking
		this.changed = {};
		this._silent = {};
		this._pending = {};
		
		this._previousProperties = _.clone(this.properties);
		
		//Go
		this.initialize.apply(this, arguments);
		
		if(this._options.autoSetReady) this.ready = true;
		
	};
	
	
	//Attach all inheritable methods to the Class prototype.
	_.extend(Class.prototype, Events, {
		
		// A hash of attributes whose current and previous value differ.
	    changed: null,
		
		// A hash of attributes that have silently changed since the last time
	    // `change` was called.  Will become pending attributes on the next call.
	    _silent: null,
	
		// A hash of attributes that have changed since the last `'change'` event
	    // began.
	    _pending: null,
	
		//Empty init
		initialize: function() {
			
		},
		//Create a new class with identical properties to this one
		clone: function() {
			return new this.constructor(this._properties)
		},
		
		destroy: function() {
			
		},
		
		resurrect: function() {
			
		},
		
	    // Check if the model is currently in a valid state. It's only possible to
	    // get into an *invalid* state if you're using silent changes.
	    isValid: function() {

	      return !this.validate(this.properties);

	    },
	
		// Return a copy of the model's properties object.
	    toJSON: function(options) {
	      return _.clone(this.properties);
	    },
	
		// Get the HTML-escaped value of a property.
	    escape: function(attr) {
	      var html;
	      if (html = this._escapedAttributes[attr]) return html;
	      var val = this.get(attr);
	      return this._escapedAttributes[attr] = _.escape(val == null ? '' : '' + val);
	    },
	
		// Returns `true` if the attribute contains a value that is not null
	    // or undefined.
	    has: function(attr) {
	      return this.get(attr) != null;
	    },
	
		// Get the value of an attribute.
	    get: function(attr) {
	      return this.properties[attr];
	    },
	
		// Set a hash of model attributes on the object, firing `"change"` unless
	    // you choose to silence it.
	    set: function(key, value, options) {
		
	      var attrs, attr, val;

	      // Handle both `"key", value` and `{key: value}` -style arguments.
	      if (_.isObject(key) || key == null) {
	        attrs = key;
	        options = value;
	      } else {
	        attrs = {};
	        attrs[key] = value;
	      }
	
	      // Extract attributes and options.
	      options || (options = {});
	      if (!attrs) return this;
	      if (attrs instanceof Class) attrs = attrs.properties;
	      if (options.unset) for (attr in attrs) attrs[attr] = void 0;

	      // Run validation.
	      if (!this._validate(attrs, options)) return false;

	      // Check for changes of `id`.
	      if (this.idAttribute in attrs) this.id = attrs[this.idAttribute];

	      var changes = options.changes = {};
	      var now = this.properties;
	      var escaped = this._escapedProperties;
	      var prev = this._previousProperties || {};

	      // For each `set` attribute...
	      for (attr in attrs) {
	        val = attrs[attr];
	        // If the new and current value differ, record the change.
	        if (!_.isEqual(now[attr], val) || (options.unset && _.has(now, attr))) {
			
	          delete escaped[attr];
	
	          (options.silent ? this._silent : changes)[attr] = true;
				
	        }
	        // Update or delete the current value.
	        options.unset ? delete now[attr] : now[attr] = val;
			
	        // If the new and previous value differ, record the change.  If not,
	        // then remove changes for this attribute.
	        if (!_.isEqual(prev[attr], val) || (_.has(now, attr) != _.has(prev, attr))) {
	          this.changed[attr] = val;
	          if (!options.silent) this._pending[attr] = true;
	        } else {
	          delete this.changed[attr];
	          delete this._pending[attr];
	        }
	      }

	      // Fire the `"change"` events.
	      if (!options.silent) this.change(options);

	      return this;
	    },

	    // Remove an attribute from the model, firing `"change"` unless you choose
	    // to silence it. `unset` is a noop if the attribute doesn't exist.
	    unset: function(attr, options) {
	      (options || (options = {})).unset = true;
	      return this.set(attr, null, options);
	    },
	
		change: function(options) {
	      options || (options = {});
	      var changing = this._changing;
	      this._changing = true;

	      // Silent changes become pending changes.
	      for (var attr in this._silent) this._pending[attr] = true;

	      // Silent changes are triggered.
	      var changes = _.extend({}, options.changes, this._silent);
	      this._silent = {};
	      for (var attr in changes) {
	        this.trigger('change:' + attr, this, this.get(attr), options);
	      }
	      if (changing) return this;

	      // Continue firing `"change"` events while there are pending changes.
	      while (!_.isEmpty(this._pending)) {
	        this._pending = {};
	        this.trigger('change', this, options);
	        // Pending and silent changes still remain.
	        for (var attr in this.changed) {
	          if (this._pending[attr] || this._silent[attr]) continue;
	          delete this.changed[attr];
	        }
	        this._previousAttributes = _.clone(this.properties);
	      }

	      this._changing = false;
	      return this;
	    },
	
		// Determine if the model has changed since the last `"change"` event.
	    // If you specify an attribute name, determine if that attribute has changed.
	    hasChanged: function(attr) {
	      if (!arguments.length) return !_.isEmpty(this.changed);
	      return _.has(this.changed, attr);
	    },

	    // Run validation against the next complete set of model attributes,
	    // returning `true` if all is well. If a specific `error` callback has
	    // been passed, call that instead of firing the general `"error"` event.
	    _validate: function(attrs, options) {

	      if (options.silent || !this.validate) return true;

	      attrs = _.extend({}, this.properties, attrs);

	      var error = this.validate(attrs, options);

	      if (!error) return true;

	      if (options && options.error) {

	        options.error(this, error, options);

	      } else {

	        this.trigger('error', this, error, options);

	      }

	      return false;
	    
		}
		
	});
	
	// The self-propagating extend function that Backbone classes use.
	var extend = function (protoProps, classProps) {

		var child = inherits(this, protoProps, classProps);

		child.extend = this.extend;

		return child;

	};
	
	// Set up inheritance for the Class.
    Class.extend = extend;

	// Helper function to get a value from a Backbone object as a property
	// or as a function.
  	var getValue = function(object, prop) {
    
		if (!(object && object[prop])) return null;
    	
		return _.isFunction(object[prop]) ? object[prop]() : object[prop];
		
  	};

	// Shared empty constructor function to aid in prototype-chain creation.
	var ctor = function(){};

	//Helper function to correctly set up the prototype chain, for subclasses.
	// Similar to `goog.inherits`, but uses a hash of prototype properties and
	// class properties to be extended.
	var inherits = function(parent, protoProps, staticProps) {
		
		var child;

	    // The constructor function for the new subclass is either defined by you
	    // (the "constructor" property in your `extend` definition), or defaulted
	    // by us to simply call the parent's constructor.
	    if (protoProps && protoProps.hasOwnProperty('constructor')) {
	      child = protoProps.constructor;
	    } else {
	      child = function(){ parent.apply(this, arguments); };
	    }

	    // Inherit class (static) properties from parent.
	    _.extend(child, parent);

	    // Set the prototype chain to inherit from `parent`, without calling
	    // `parent`'s constructor function.
	    ctor.prototype = parent.prototype;
	    child.prototype = new ctor();

	    // Add prototype properties (instance properties) to the subclass,
	    // if supplied.
	    if (protoProps) _.extend(child.prototype, protoProps);

	    // Add static properties to the constructor function, if supplied.
	    if (staticProps) _.extend(child, staticProps);

	    // Correctly set child's `prototype.constructor`.
	    child.prototype.constructor = child;

	    // Set a convenience property in case the parent's prototype is needed later.
	    child.__super__ = parent.prototype;

	    return child;
	
    };

	
})();