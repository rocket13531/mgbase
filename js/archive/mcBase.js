
var Mgbase = Backbone.Model.extend({
	constructor: function(attributes, options) {
		
		//Options: 
		//autoSetReady: true/false (Determines whether or not ready class state will be )
		
		var _mgbase = this;
		
		_mgbase.alive = false;
		
		_mgbase.ready = false;
		
		_mgbase.intervals = [];
		
		_mgbase.timeouts = [];
		
		_mgbase.attributes = attributes = _.extend({className: 'Mgbase', autoSetReady: true, debugMode: true}, attributes);
		
		_mgbase.setTimeout(function() {
			
			if(!_mgbase.attributes.autoSetReady) _mgbase.logError('Error!')
			
		}, 1);
		
		Backbone.Model.prototype.constructor.apply(_mgbase, arguments);
	    
	},
	
	alert: function(msg) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		alert(_mgbase.attributes.className + ' [id: '+_mgbase.cid + ']: ' + msg);

	},

	log : function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		if ( typeof msg === 'object') {

			obj = msg;

			msg = '';

		}

		if ( typeof obj !== 'object')
			obj = '';

		if ( typeof console === 'object' && typeof console.log === 'function') {
			console.log(_mgbase.cid + ': ' + msg, obj);
		}

	},

	logError : function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		_mgbase.log('Error - ' + msg, obj);

	},

	logWarning : function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		_mgbase.log('Warning - ' + msg, obj);

	},

	logNotice : function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		_mgbase.log('Notice - ' + msg, obj);

	},

	setInterval : function(fn, duration) {

		var _mgbase = this;

		if ( typeof fn !== 'function')
			return _mgbase.logError('setInterval() fn provided is not a function');

		var _duration = duration || 1;

		var interval = setInterval(fn, duration);

		_mgbase.intervals.push(interval);

		return interval;
	},

	clearInterval : function(id) {

		var _mgbase = this;

		window.clearInterval(id);

	},

	clearAllIntervals : function() {

		var _mgbase = this;

		if (_mgbase.intervals.length === 0)
			return false;

		$.each(_mgbase.intervals, function() {
			_mgbase.clearInterval(this);
		});

		_mgbase.intervals = [];

		return true;

	},

	setTimeout : function(fn, duration) {

		var _mgbase = this;

		if ( typeof fn !== 'function')
			return _mgbase.logError('setTimeout() fn provided is not a function');

		var _duration = duration || 1;

		var timeout = setTimeout(fn, duration);

		_mgbase.timeouts.push(timeout);

		return timeout;
	},

	clearTimeout : function(id) {

		var _mgbase = this;

		clearTimeout(id);

	},

	clearAllTimeouts : function() {
		var _mgbase = this;

		if (_mgbase.timeouts.length === 0)
			return false;

		$.each(_mgbase.timeouts, function() {

			_mgbase.clearTimeout(this);

		});

		_mgbase.timeouts = [];

		return true;

	},
	
});