//Class Description: FadeSlider extends the Slider class to configure by default for a fade slideshow.

MG.FadeSlider = MG.Slider.extend({

	defaults: function() {

        return _.extend(_.result(MG.FadeSlider.__super__, 'defaults'), {
        	type: 'FadeSlider',
			mode: 'fade',
			setActive: false
        });

    }

});