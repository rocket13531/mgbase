
/*MG.Test = MG.Mgbase.extend({

	defaults: {
		className: 'Test',
		dog: 'bark'
    }
	
});

MG.Test2 = MG.Test.extend({

	defaults: function() {

        return _.extend(_.result(MG.Test2.__super__, 'defaults'), {
        	className: 'TestClass',
			test: 'Test Class 2',
			newTest: 'New Test'
        });
    
    }
	
})*/


$(function() {

	/*MG.carouselSlider = new MG.CarouselSlider({viewport: '#carousel-slider', auto: true});

	MG.fadeSlider = new MG.FadeSlider({viewport: '#fade-slider', auto: true});*/
	
	MG.classCrawler = new MG.ClassCrawler();

	//Create Site Group
	MG.siteGroup = new MG.ClassGroup({
		sourceEl: '#container',
		addChildren: [
			{
				class: 'ClassGroup',
				attributes: {
					groupType: 'headerGroup',
					activate: false,
					sourceEl: '#header'
				}
			},
			{
				class: 'ClassGroup',
				attributes: {
					groupType: 'sectionsGroup',
					activate: false,
					sourceEl: '#sections',
					crawlSelf: false
				}
			},
			{
				class: 'ClassGroup',
				attributes: {
					groupType: 'footerGroup',
					activate: false,
					sourceEl: '#footer'
				}
			}
		],
		addChildrenByEl: {filterParents: '#sections, #footer, #header'},
		groupType: 'siteGroup'
	});

	//Unnecessary to create these references but helpful!
	MG.headerGroup = MG.siteGroup.getChildByGroupType('headerGroup');

	MG.sectionsGroup = MG.siteGroup.getChildByGroupType('sectionsGroup');

	MG.footerGroup = MG.siteGroup.getChildByGroupType('footerGroup');

	MG.sectionGroup = [];

	$('section').each(function(i) {
		MG.sectionGroup[i] = MG.sectionsGroup.addChild({
			class: 'ClassGroup',
			attributes: {
				activate: false,
				parentActivated: true,
				sourceEl: $(this),
				groupType: 'sectionGroup'
			}
		})
	});

	//Create Header Group
	/*MG.headerGroup = MG.siteGroup.addChild({
		class: 'ClassGroup',
		attributes: {
			activate: false,
			sourceEl: '#header',
			groupType: 'headerGroup'
		}
	});

	//Create Sections Group
	MG.sectionsGroup = MG.siteGroup.addChild({
		class: 'ClassGroup',
		attributes: {
			activate: false,
			sourceEl: '#sections',
			groupType: 'sectionsGroup',
			crawlSelf: false
		}
	});*/

	//Add each section as a child to MG.sectionsGroup
	/*$('section').each(function() {
		MG.sectionsGroup.addChild({
			class: 'ClassGroup',
			attributes: {
				activate: false,
				parentActivated: false,
				sourceEl: $(this),
				groupType: 'sectionGroup'
			}
		})
	});*/

	//Create Footer Group
	/*MG.footerGroup = MG.siteGroup.addChild({
		class: 'ClassGroup',
		attributes: {
			activate: false,
			sourceEl: '#footer',
			groupType: 'footerGroup'
		}
	});*/



	//

	/*MG.siteGroup = new MG.ClassGroup({
		sourceEl: '#group',
		groupType: 'SiteGroup',
		addChildren: [{ 
			class: 'ClassGroup', 
			attributes: {
				sourceEl: '#group .overlays',
				addChildrenByEl: {
					filterParents: '.overlay'
				},
				groupType: 'SiteOverlayGroups'
			}
		}],
		addChildrenByEl: {el: '#group', filterParents: '.overlays' }
	});

	MG.siteOverlays = MG.siteGroup.getChildren({groupTypes: 'SiteOverlayGroups'})[0];

	$('#group .overlay').each(function() {

		MG.siteOverlays.addChildren([{
			class: 'ClassGroup',
			attributes: {
				activate: false,
				sourceEl: $(this),
				addChildrenByEl: { 
					andSelf: false 
				},
				groupType: 'SiteOverlayGroup'
			}
		}]);

	});*/


	<!--

		/* 

		var carousel = MG.siteGroup.getChild({el: , groupType: '', cid: '', construct: true });


		*/

	-->

});

