MG.Spinner = MG.Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.Spinner.__super__, 'defaults'), {
            type: 'Spinner',
            setReady: false,
            requires: [jQuery],
            container: null,
            target: 'img',
            dragRegion: null,
            hiResPath: '',
            loResPath: '',
            autoPlay: true,
            autoPlayDelay: 500,
            frames: 0,
            initIndex: 0,
            direction: 1, //-1 backwards, 0 shortest distance, 1 forward
            keyframes: [/*{frame: 0, name: 'start'}*/],
            frameInterval: 70,
            _currentIndex: 0
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	_showFrame: function(index) {

	},

	_nextFrame: function() {

	},

	_prevFrame: function() {

	},

	_createPreload

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		}

	},

	play: function(options) {

		var _mgbase = this;

	},

	pause: function(options) {

		var _mgbase = this;

	},

	seekTo: function(index, options) {

		var _mgbase = this;

	},

	setLoop: function(boolean) {

		var _mgbase = this;

	},

	disableControls: function() {

		var _mgbase = this;

	},

	enableControls: function() {

		var _mgbase = this;

	},

	addKeyframe: function(index, name) {

		var _mgbase = this;

	},

	gotoKeyframe: function(keyframe, options) {

		var _mgbase = this;

	}

});