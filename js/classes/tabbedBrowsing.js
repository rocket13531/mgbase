var TabbedBrowsing = Mgbase.extend({

	//Default attributes which can be overridden by the class user
	defaults: {
		type: 'TabbedBrowsing', //IMPORTANT - set this value to the Class Name.
		context: '#tabbed-browsing-context',
		viewport: '#tabbed-browsing-viewport',
		items: '.tabbed-browsing-item',
		prevButton: '',
		nextButton: '',
		navClass: 'tabbed-browsing-nav',
		navItemClass: 'tabbed-browsing-nav-item',
		activeClass: 'tabbed-item-active',
		wrapperClass: 'tabbed-browsing-wrapper',
		appendNavTo: null,
		insertNavAfter: '.tabbed-browsing-items',
		insertNavBefore: '',
		circular: true,
		navTitlesHTML: [],
		bindNavClick: true,
		bindNavHover: false,
		toggleShow: false,
		fadeDuration: 400,
		initIndex: 0,
		setInitStyles: true,
		drawNav: true,
		drawPrevNext: false,
		noAnimate: false,
		changeTarget: false,
		useOpacity: true
	},
	
	//Internal Class Variables
	_globals: {
		currentIndex: null,
		numItems: 0,
		viewportWidth: 0,
		viewportHeight: 0,
		insertNavAt: null,
		navItems: null,//'.'+_mgbase.attributes.navItemClass,
		initialized: false,
		alwaysSnap: false,
		visible: false
	},

	validate: function(attrs, options) {

		var _mgbase = this;

		if(_.has(attrs, 'viewport')) if(attrs.viewport.length !== 1) return 'Viewport length is invalid.';

		if(_.has(attrs, 'context')) if(attrs.context.length !== 1) return 'Context length is invalid.';  

		if(_.has(attrs, '$items')) if(typeof attrs.$items !== 'object' || attrs.$items.length < 1) return 'No tabbed browsing items found';

		if(_.has(attrs, 'numItems')) if(attrs.numItems < 1) return 'Num items must be greater than 0';

		if(_.has(attrs, 'insertNavAt')) if(attrs.insertNavAt.length !== 1) return 'insertNavAt length is invalid';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		_mgbase._config();

		_mgbase.checkValid(_mgbase.attributes);

		_mgbase._setup();

		return;

		_mgbase._setBinds();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

		_mgbase.attributes.viewport = (_mgbase.attributes.viewport) ? $(_mgbase.attributes.viewport) : _mgbase.attributes.sourceEl;

		_mgbase.attributes.context = _mgbase.attributes.viewport.parents(_mgbase.attributes.context);

		_mgbase.attributes.$items = _mgbase.attributes.viewport.find(_mgbase.attributes.items);

		_mgbase.attributes.numItems = _mgbase.attributes.items.length;

		_mgbase.attributes.appendNavTo = (_mgbase.attributes.appendNavTo) ? _mgbase._globals.context : $(_mgbase.attributes.appendNavTo);

		_mgbase.attributes.alwaysSnap = _mgbase.attributes.noAnimate || _mgbase._globals.alwaysSnap;

		_mgbase.attributes.drawNav = (_mgbase.attributes.numItems <= 1) ? false : true;

		if(_mgbase.attributes.appendNavTo.length !== 1) {
			
			_mgbase.attributes.insertNavAfter = (_mgbase.attributes.insertNavBefore) ? false : true;
		
			_mgbase.attributes.insertNavAt = _mgbase.attributes.insertNavBefore || _mgbase.attributes.insertNavAfter;
		
			_mgbase.attributes.insertNavAt = $(_mgbase.attributes.insertNavAt, _mgbase.attributes.context);
		
			if(_mgbase.attributes.insertNavAt.length < 1) {

				_mgbase.attributes.insertNavAt = _mgbase.attributes.context;

			}
			
		}

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;


		//TODO: INITIALLY HIDE!
		/*_mgbase.attributes.items.each(function() {
			
			_mgbase.hideObj({obj: $(this)});
			
		});*/


		if(_mgbase.get('drawNav')) _mgbase._drawNav();

		/*
		if(_properties.drawNav) _drawNav();
		
		if(_properties.drawPrevNext) {
			
			_properties.prevButton = $('.tabbed-browsing-prev-item a');
			
			_properties.nextButton = $('.tabbed-browsing-next-item a');
			
		} else {
			
			_properties.prevButton = $(_properties.prevButton)
			
			_properties.nextButton = $(_properties.nextButton);
			
		}
		
		_globals.nav = _globals.context.find('.' + _properties.navClass);
		
		_globals.navItems = _globals.context.find(_globals.navItems);
		
		_setupItems();
		*/

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setReactivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

		_mgbase.binds.setBindNav(_mgbase);

		_mgbase.binds.setBindPrevNext(_mgbase);

	},

	_drawNav: function() {

		var _mgbase = this;
		
		var navHTML = '<ul class="'+_mgbase.attributes.navClass+'">';
		
		var firstLastClass = '';
		
		if(_mgbase.attributes.drawPrevNext) navHTML += '<li class="tabbed-browsing-prev-item tabbed-browsing-prev-next-item"><a href="#"><</a></li>';
		
		$.each(_mgbase.attributes.$items, function(i) {
			
			firstLastClass = '';

			if(i === 0) firstLastClass = 'first';

			if(i === _mgbase.attributes.numItems - 1) firstLastClass = 'last';
			
			var _navTitle = _mgbase.attributes.navTitlesHTML[i] || (i + 1);
			
			navHTML += '<li class="'+_mgbase.attributes.navItemClass+' '+_mgbase.attributes.navItemClass+'-'+(i+1)+' '+firstLastClass+'" data-index="'+i+'"><a href="#"><span>'+_navTitle+'</span></a></li>';
			
		});
		
		if(_mgbase.attributes.drawPrevNext) navHTML += '<li class="tabbed-browsing-next-item tabbed-browsing-prev-next-item"><a href="#">></a></li>';
		
		navHTML += '</ul>';
		
		if(_mgbase.attributes.appendNavTo.length === 1) {
			
			_mgbase.attributes.appendNavTo.append(navHTML);
			
		} else if(_mgbase.attributes.insertNavAfter) {
			
			_mgbase.attributes.insertNavAt.after(navHTML);
			
		} else {
			
			_mgbase.attributes.insertNavAt.before(navHTML);
			
		} 
		
	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setReactivateEvent: function(ctx) {

			var _mgbase = ctx;

			_mgbase.on('reactivate', function(e, data) {

				//On reactivate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(ctx) {

			var _mgbase = ctx;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		},

		setBindNav: function(ctx) {

			var _mgbase = ctx;
		
			$.each(_mgbase.attributes.navItems, function(i) {
				
				var $this = $(this);
				
				if(_mgbase.attributes.bindNavClick) {
					
					$this.bind('click tap', function(e) {

						e.preventDefault();
						
						_mgbase.show(i);

					});
				}
				
				if(_mgbase.attributes.bindNavHover) {

					$this.hover(function() {

						_mgbase.show(i);

					}, function() {
						
						if(_mgbase.attributes.toggleShow) _mgbase.hide();
						
					});

				}
				
			});
			
		},
		
		setBindPrevNext: function(ctx) {

			var _mgbase = ctx;
		
			if(_mgbase.attributes.prevButton.length === 1) {
				
				_mgbase.attributes.prevButton.bind('click tap', function(e) {
					
					e.preventDefault();
					
					if(_mgbase.attributes.currentIndex === null) _mgbase.attributes.currentIndex = 0;
					
					_mgbase.show(_mgbase.attributes.currentIndex - 1);
					
				});
				
			}
			
			if(_mgbase.attributes.nextButton.length === 1) {
				
				_mgbase.attributes.nextButton.bind('click tap', function(e) {
					
					e.preventDefault();
					
					if(_mgbase.attributes.currentIndex === null) _mgbase.attributes.currentIndex = 0;
					
					_mgbase.show(_mgbase.attributes.currentIndex + 1);
					
				});
				
			}
			
		}

	},

	publicMethod: function(msg) {

		var _mgbase = this;

	}

});