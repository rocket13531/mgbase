var GroupManager = Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(GroupManager.__super__, 'defaults'), {
            className: 'GroupManager',
            classAttr: 'data-crawler-invoke',
            classConstructedAttr: 'data-crawler-contructed',
            groupIdAttr: 'data-crawler-group-id',
            groupIdPrefix: 'group_',
            ignoreEls: '',
            filterParents: '',
			_groups: []
        });
    
    },

    //Attribute validations
	validate: function(attrs, options) {
	
		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';
		if(_.has(attrs, 'classId')) if(typeof attrs.classId !== 'string') return 'classId must be a string';
		if(_.has(attrs, 'classAttr')) if(typeof attrs.classAttr !== 'string') return 'classAttr must be a string';
		if(_.has(attrs, 'classConstructedAttr')) if(typeof attrs.classConstructedAttr !== 'string') return 'classConstructedAttr must be a string';
		if(_.has(attrs, 'groupIdAttr')) if(typeof attrs.groupIdAttr !== 'string') return 'groupIdAttr must be a string';
		if(_.has(attrs, '_groups') && _groups) if(typeof _groups !== 'object') return '_groups must be an object';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		}

	},

	//If is a group
	_isGroup: function(groupId) {

		var _mgbase = this;

		var _ret = false;

		//TODO: I believe _.some will be better here
		_.each(_mgbase._groups, function() {

			if(this.groupId === groupId) _ret = true;

		})

		return _ret;

	},

	//Group must be the group object
	_deactivateGroup: function(groupObj) {

		var _mgbase = this;

		if(typeof group !== 'object') _mgbase.logError('group provided to _deactiveGroup is invalid.');

		_.each(group.instances, function() {

			//If is active lets deactivate it.
			if(this.get('active')) this.deactivate();

		});

	},

	//Group must be the group object
	_activateGroup: function(groupObj) {

		var _mgbase = this;

		if(typeof group !== 'object') _mgbase.logError('group provided to _activeGroup is invalid.');

		_.each(group.instances, function() {

			//If not active lets activate it.
			if(!this.get('active')) this.activate();

		});

	},

	//PUBLIC METHODS

	getGroup: function(groupId) {

		var _mgbase = this;

		var _group = null;

		//TODO: Must be a better way to do this
		_.each(_mgbase._groups, function() {

			if(this.groupId === groupId) _ret = this;

		});

		if(!_group) _mgbase.logError('groupId ['+groupId+']not found in _getGroupById.');

		return _ret;

	},

	addGroup: function(group, options) {

		var _group = group;

		var _crawl = true;

		if(_group instanceof jQuery || _group.nodeType) _group = $(_group);

		else if(typeof _group === 'object') _crawl = false;
		
		else _mgbase.error('Unexpected error in addGroup');

		if(_group.length === 0) _mgbase.error('group provided to addGroup is invalid.');

		var _options = _.extend({
			groupId: _.uniqueId(_mgbase.attributes.groupIdPrefix),
			activate: true,
			filterParents: _mgbase.attributes.filterParents, 
			ignoreEls: _mgbase.attributes.ignoreEls,
			classObjects: null,/*[{groupId: FadeSlider, instances: [] }]*/
		}, options);

		if(_crawl) {

		} else {

			_mgbase.activateGroup(_group);

		}

	},
	
	getGroupByEl: function(el) {

		var _mgbase = this;

		var $el = $(el);

		if($el.length !== 1) return _mgbase.logError('El provided to getGroupByEl is invalid.');

		//TODO
		//PSUEDO CODE:
		//If El has class "crawled", get group by Id and return

		var $classes = $el.find('['+_mgbase.attributes.classAttr+']').not(_options.ignoreEls) || null;

		//If we want to exclude classes with a particular parent.
		if(_options.filterParents) $classes = $classes.filter(function() { return $(this).parents(_options.filterParents).length < 1 });

		if(!$classes)  

	},

	addGroupByEl: function(el, options) {

		var _mgbase = this;

		var $el = $(el);

		var _options = _.extend({
			groupId: _.uniqueId(_mgbase.attributes.groupIdPrefix), 
			activate: true, 
			group: [],
			filterParents: '', 
			ignoreEls: ''
		}, options);

		if($el.length !== 1) return _mgbase.logError('El provided to addGroupByEl is invalid.');


	},
	//deleteGroup: function() {}, ??






	//Group can be either an element, selector, or a groupId
	activateGroup: function(group, options) {

		var _mgbase = this;

		var _crawl = true;

		var _group = null;

		var _options = _.extend({
			groupId: _.uniqueId(_mgbase.attributes.groupIdPrefix),
			filterParents: '', 
			ignoreEls: ''
		}, options);

		if(_mgbase.isGroup(group)) _crawl = false;

		//If we are activating the group via the dom, then lets crawl it.
		if(_crawl) {

			var $el = $(group);

			if($el.length !== 1) return _mgbase.logError('group provided is invalid in activateGroup.');

			var $classes = $el.find('['+_mgbase.attributes.classAttr+']').not(_options.ignoreEls) || null;

			//If we want to exclude classes with a particular parent.
			if(_options.filterParents) $classes = $classes.filter(function() { return $(this).parents(_options.filterParents).length < 1 });

			if(!$classes)  

		} else {
			
			//Since we are not crawling we are getting the group by id.
			_group = _mgbase.getGroupById(group);

			_mgbase.activateGroup(_group);

		}

	},

	//Group can be either an element, selector, or a groupId
	deactivateGroup: function(group, options) {

		var _crawl = true;

		if(_isGroup(group)) _crawl = false;

	},

	reactivateGroup: function(group, options) {

	}

});