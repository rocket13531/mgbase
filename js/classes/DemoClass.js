MG.DemoClass = MG.Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.DemoClass.__super__, 'defaults'), {
            type: 'DemoClass',
            bgcolor: 'blue'
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();

		_mgbase.show();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

		_mgbase.attributes.$sourceEl.append('<p>'+_mgbase.attributes.type+'</p>');

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log("I'm awake.");

				_mgbase.show();

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Good Night.');

				_mgbase.hide();

			});
			
		}

	},

	show: function() {

		var _mgbase = this;

		_mgbase.attributes.$sourceEl.css({background: _mgbase.attributes.bgcolor});

	},

	hide: function() {

		var _mgbase = this;

		_mgbase.attributes.$sourceEl.css({background: '#fff'});

	}

});