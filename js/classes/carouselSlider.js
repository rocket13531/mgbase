//Class Description: FadeSlider extends the Slider class to configure by default for a carousel.

MG.CarouselSlider = MG.Slider.extend({

	defaults: function() {

        return _.extend(_.result(MG.CarouselSlider.__super__, 'defaults'), {
        	type: 'CarouselSlider',
			mode: 'horizontal',
			setReady: false,
			setActive: false,
			debugMode: true,
			sourceEl: '#fade-slider'
        });
    
    }


});