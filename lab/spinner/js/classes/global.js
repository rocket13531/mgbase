MG.Global = MG.Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.Global.__super__, 'defaults'), {
            type: 'Global'
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

		MG.instances.spinner = new MG.Spinner();

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		}

	},

	publicMethod: function() {

		var _mgbase = this;

	}

});