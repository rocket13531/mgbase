
(function() {
	
	// Save a reference to the global object
	var root = this;
	
	// The top-level namespace
	var Rockbase = root.Rockbase = {};
	
	var Globals = Rockbase.Globals = {
		els: {
			window: $(window),
			body: $('body')
		}
	};
	
	// Base Class
	var Class = Rockbase.Class = function(properties) {

		//PRIVATE VARS

		var _baseClass = this;

		var _defaults = {
			sourceEl : null,
			classId : null,
			classType : null,
			msg : 'This is the default base message',
			classDependents : [],
			triggerSrc : null,
			instanceName : '',
			autoCallCreateComplete : true,
			browserData : Rockbase.Utils.getBrowserData().isIE,
			instanceIdAttr : 'data-dd-class-instance',
			debugMode: true,
			onCreateComplete : function() {
			},
			onLoadComplete : function() {
			},
			onDestroyComplete : function() {
			}
		};

		//PUBLIC VARS

		_baseClass.properties = jQ.extend(_defaults, properties);

		_baseClass.globals = {
			classId : '',
			alive : false,
			instantiated : false,
			setupComplete : false,
			createComplete : false,
			loadComplete : false,
			classDependentsInstantiated : false,
			classDependentsInstances : [],
			classType : '',
			timeouts : [],
			intervals : [],
			alwaysSnap : false,
			customCreate : function() {
			},
			customCreateComplete : function() {
			},
			customDestroy : function() {
			},
			customDestroyComplete : function() {
			},
			customResurrect : function() {
			},
			customLoadComplete : function() {
			}
		};

		//PRIVATE METHODS

		var _init = function() {

			_config();

		};

		var _config = function() {

			_baseClass.globals.classType = _baseClass.properties.classType || _baseClass.properties.classId;

			_baseClass.properties.sourceEl = jQ(_baseClass.properties.sourceEl);

			_baseClass.globals.classId = _baseClass.properties.classId || _baseClass.properties.instanceName;

			_baseClass.globals.alwaysSnap = Rockbase.Utils.getBrowserData().isIE;

		};

		var _setup = function() {

		};

		//PUBLIC METHODS

		_baseClass.getConstants = function() {
			return _CONSTANTS;
		};

		_baseClass.isAlive = function() {
			return _baseClass.globals.alive;
		};

		_init();

	};

	Rockbase.Class.prototype = {

		addMethod : function(name, fn) {

			var _baseClass = this;

			if (!name || typeof fn !== 'function')
				return _baseClass.logError('addMethod() parameters passed in are invalid');

			if (!_baseClass[name])
				_baseClass[name] = fn;

		},

		create : function(options) {

			var _baseClass = this;

			var _options = jQ.extend({
				createStart : function() {
				},
				createComplete : function() {
				}
			}, options);

			if (_baseClass.globals.alive)
				return _baseClass.logWarning('Class.create cannot create class, it is already created.');

			_baseClass.globals.alive = true;

			_baseClass.createStart(_options.createStart);

			_baseClass.createClassDependents();

			if (_baseClass.properties.sourceEl.length > 0)
			
				_baseClass.properties.sourceEl.attr(_baseClass.properties.instanceIdAttr, _baseClass.globals.classId);

			if ( typeof _baseClass.globals.customCreate === 'function' && !_baseClass.globals.instantiated) {

				_baseClass.globals.customCreate();

			} else if ( typeof _baseClass.globals.customResurrect === 'function' && _baseClass.globals.instantiated) {
				//if class already loaded, run custom instance resurrection function
				_baseClass.globals.customResurrect();

			}

			_baseClass.globals.setupComplete = true;

			if (_baseClass.properties.autoCallCreateComplete === true) {

				_baseClass.createComplete(_options.createComplete);

			}

			return true;
		},

		destroy : function(options) {

			var _baseClass = this;

			var _options = jQ.extend({
				destroyStart : function() {
				},
				destroyComplete : function() {
				}
			}, options);

			if (!_baseClass.globals.alive)
				return _baseClass.logWarning('Class.destroy cannot destroy class, it has not been created.');

			//Lets run destroy start callback and wait for possible animations to complete before continuing to shut down
			_baseClass.destroyStart(function() {

				_baseClass.destroyClassDependents();

				if ( typeof _baseClass.globals.customDestroy === 'function')
					_baseClass.globals.customDestroy();

				_baseClass.globals.alive = false;

				_baseClass.destroyComplete(_options.destroyComplete);

			});

			_baseClass.clearAllTimeouts();

			_baseClass.clearAllIntervals();

			return true;

		},

		createClassDependents : function() {

			//Dynamically run through all dependencies provided and create an instance for each.

			var _baseClass = this;

			//create class dependents

			if (_baseClass.globals.classDependentsInstantiated) {

				jQ.each(_baseClass.globals.classDependentsInstances, function(i) {

					var _this = this;

					_this.create();

				});

				return true;
			}

			//instantiate/create class dependents

			if (_baseClass.properties.classDependents.length < 1)
				return false;

			jQ.each(_baseClass.properties.classDependents, function() {

				Rockbase.Utils.extend(this.obj, Rockbase.Class);

				var _properties = jQ.extend({
					onCreateComplete : function() {
						setTimeout(function() {
							if (!_baseClass.globals.createComplete)
								_baseClass.createComplete();
						}, 1);
					}
				}, this.properties);

				/*Look above, very important BUT...needs some reconsideration perhaps*/

				_baseClass.globals.classDependentsInstances.push(new this.obj(_properties));

			});

			_baseClass.globals.classDependentsInstantiated = true;

			return true;

		},

		destroyClassDependents : function() {

			var _baseClass = this;

			if (_baseClass.globals.classDependentsInstances.length < 1)
				return false;

			jQ.each(_baseClass.globals.classDependentsInstances, function() {

				var _this = this;

				_this.destroy();

			});

			return true;

		},

		classDependentsStatus : function() {

			var _baseClass = this;

			//if(_baseClass.properties.classDependents)

			if (_baseClass.properties.classDependents.length === 0)
				return true;

			var _ret = {
				setupComplete : true,
				loadComplete : true
			};

			jQ.each(_baseClass.properties.classDependents, function(i) {
				if (!_baseClass.globals.classDependentsInstances[i].globals.setupComplete)
					_ret = jQ.extend(_ret, {
						setupComplete : false
					});
				if (!_baseClass.globals.classDependentsInstances[i].globals.loadComplete)
					_ret = jQ.extend(_ret, {
						loadComplete : false
					});
			});

			return _ret;

		},

		getInstanceName : function() {

			var _baseClass = this;

			if (_baseClass.properties.instanceName) {
				return _baseClass.properties.instanceName;
			} else {
				return false;
			}

		},

		createStart : function(fn) {

			var _baseClass = this;

			if ( typeof fn === 'function')
				fn();
		},

		createComplete : function(fn) {

			_baseClass = this;

			if (_baseClass.globals.createComplete)
				return false;
			/*_baseClass.logWarning('createComplete() has already been successfully executed...is this a necessary warning?');*/

			if (_baseClass.globals.classDependentsInstances.length > 0)
				_baseClass.loadCompleteChecker();

			if (_baseClass.globals.classDependentsInstances.length > 0 && !_baseClass.classDependentsStatus().setupComplete) {
				return false;
			}

			var _fn = function() {
				if ( typeof fn === 'function')
					fn();

				_baseClass.properties.onCreateComplete();

				_baseClass.globals.customCreateComplete();

				_baseClass.globals.createComplete = true;

				_baseClass.globals.instantiated = true;

				jQ(window).trigger('classCreateComplete', {
					classId : _baseClass.properties.classId,
					classType : _baseClass.globals.classType
				});

				if (_baseClass.isLoaded())
					_baseClass.globals.loadComplete = true;
			};

			if ( typeof _fn === 'function')
				_fn();

			return true;

		},

		loadComplete : function(fn) {

			_baseClass = this;

			var _fn = function() {
				if ( typeof fn === 'function')
					fn();

				_baseClass.properties.onLoadComplete();

				_baseClass.globals.customLoadComplete();

				_baseClass.globals.loadComplete = true;

			};

			_fn();

		},

		destroyStart : function(fn) {

			_baseClass = this;

			jQ(window).trigger('classDestroyStart', {
				classId : _baseClass.properties.classId,
				classType : _baseClass.globals.classType
			});

			if ( typeof fn === 'function')
				fn();

		},

		destroyComplete : function(fn) {

			_baseClass = this;

			var _fn = function() {

				if ( typeof fn === 'function')
					fn();

				_baseClass.properties.onDestroyComplete();

				_baseClass.globals.customDestroyComplete();

				_baseClass.globals.createComplete = false;

				jQ(window).trigger('classDestroyComplete', {
					classId : _baseClass.properties.classId,
					classType : _baseClass.globals.classType
				});
			};

			if ( typeof fn === 'function')
				_fn();
		},

		trigger : function(name, data) {

			_baseClass = this;

			var jQwindow = jQ(window);

			var _data = data || null;

			var _name = name || '';

			jQwindow.trigger(_baseClass.globals.classType + name, data);

		},

		isLoaded : function() {

			_baseClass = this;

			if (!_baseClass.globals.createComplete)
				return false;
			else if (_baseClass.globals.classDependentsInstances < 1)
				return true;
			else if (_baseClass.classDependentsStatus().loadComplete)
				return true;
			else
				return false;
		},

		loadCompleteChecker : function() {

			var _baseClass = this;

			var __baseClass = _baseClass;

			var interval = __baseClass.setInterval(function() {

				if (__baseClass.isLoaded()) {
					__baseClass.clearInterval(interval);
					_baseClass = __baseClass;
					__baseClass.loadComplete();

				}

			}, 60);
		},
	
		alert: function(msg) {
	
			var _baseClass = this;
		
			if(!_baseClass.properties.debugMode) return false;
		
			alert(_baseClass.globals.classId + ': ' + msg);
		
		},

		log : function(msg, obj) {

			var _baseClass = this;
		
			if(!_baseClass.properties.debugMode) return false;

			if ( typeof msg === 'object') {

				obj = msg;

				msg = '';

			}

			if ( typeof obj !== 'object')
				obj = '';

			if ( typeof console === 'object' && typeof console.log === 'function') {
				console.log(_baseClass.globals.classId + ': ' + msg, obj);
			}

		},

		logError : function(msg, obj) {

			var _baseClass = this;
		
			if(!_baseClass.properties.debugMode) return false;

			_baseClass.log('Error - ' + msg, obj);

		},

		logWarning : function(msg, obj) {

			var _baseClass = this;
		
			if(!_baseClass.properties.debugMode) return false;

			_baseClass.log('Warning - ' + msg, obj);

		},

		logNotice : function(msg, obj) {

			var _baseClass = this;

			if(!_baseClass.properties.debugMode) return false;

			_baseClass.log('Notice - ' + msg, obj);

		},

		setInterval : function(fn, duration) {

			var _baseClass = this;

			if ( typeof fn !== 'function')
				return _baseClass.logError('setInterval() fn provided is not a function');

			var _duration = duration || 1;

			var interval = setInterval(fn, duration);

			_baseClass.globals.intervals.push(interval);

			return interval;
		},

		clearInterval : function(id) {

			var _baseClass = this;

			window.clearInterval(id);

		},

		clearAllIntervals : function() {

			var _baseClass = this;

			if (_baseClass.globals.intervals.length === 0)
				return false;

			jQ.each(_baseClass.globals.intervals, function() {
				_baseClass.clearInterval(this);
			});

			_baseClass.globals.intervals = [];

			return true;

		},

		setTimeout : function(fn, duration) {

			var _baseClass = this;

			if ( typeof fn !== 'function')
				return _baseClass.logError('setTimeout() fn provided is not a function');

			var _duration = duration || 1;

			var timeout = setTimeout(fn, duration);

			_baseClass.globals.timeouts.push(timeout);

			return timeout;
		},

		clearTimeout : function(id) {

			var _baseClass = this;

			clearTimeout(id);

		},

		clearAllTimeouts : function() {
			var _baseClass = this;

			if (_baseClass.globals.timeouts.length === 0)
				return false;

			jQ.each(_baseClass.globals.timeouts, function() {

				_baseClass.clearTimeout(this);

			});

			_baseClass.globals.timeouts = [];

			return true;

		},

		loadScript : function(src) {

			var _baseClass = this;

			if (jQ('script').attr('src') === src)
				return false;

			//Load player api asynchronously.
			var tag = document.createElement('script');
			tag.src = src;
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			return true;

		}
	};

	
	Rockbase.addClass = function(name, arg2, arg3) {
		
		var _options = {extend: Rockbase.Class}; 
		
		var _fn = null;
		
		if(typeof arg2 !== 'function' && typeof arg2 === 'object') {
			_options = $.extend(_options, arg2);
		} else if(typeof arg2 === 'function') {
			_fn = arg2;
		} else if(arg3 && typeof arg3 === 'function') {
			
		}
		
	};
	
	//Utils
	
	var Utils = Rockbase.Utils = {
		
		
		extend : function(subClass, superClass) {

			var F = function() {
			};

			if ( typeof subClass !== 'function')
				return console.log('Error: subClass provided extend does not exist');

			F.prototype = superClass.prototype;
			subClass.prototype = new F();
			subClass.prototype.constructor = subClass;

			subClass.superclass = superClass.prototype;

			if (superClass.prototype.constructor == Object.prototype.constructor) {
				superClass.prototype.constructor = superClass;
			}
			
			alert('SuperClass: ', subClass.prototype)
		},

		getUserAgent : function() {
			// Device detection object
			var Device = {};

			Device.UA = navigator.userAgent;

			Device.appleDevice = false;

			Device.Types = ["iPhone", "iPod", "iPad"];

			for (var d = 0; d < Device.Types.length; d++) {
				var t = Device.Types[d];
				Device[t] = !!Device.DD.match(new RegExp(t, "i"));
				Device.appleDevice = Device.appleDevice || Device[t];
			}

			// is this an Device device?
			return Device;

		},

		getBrowserData : function() {

			var browserData = {
				isIE : false,
				isIE6 : false,
				isIE7 : false,
				isIE8 : false,
				isIE9 : false,
				isLtIE9 : false,
				features : {
					csstransitions : false,
					noOpacity : false
				}
			};

			var jQhtml = jQ('html');

			browserData.isIE = jQhtml.hasClass('ie');

			browserData.isIE6 = jQhtml.hasClass('ie6');

			browserData.isIE7 = jQhtml.hasClass('ie7');

			browserData.isIE8 = jQhtml.hasClass('ie8');

			browserData.isLtIE9 = jQhtml.hasClass('lt-ie9');

			browserData.features.csstransitions = jQhtml.hasClass('csstransitions');

			browserData.features.noOpacity = jQhtml.hasClass('no-opacity');

			return browserData;

		},

		arrayShiftTo : function(array, index, targetIndex) {

			if ( typeof array !== 'object')
				return console.log('Error: array provided is not of type object');

			if ( typeof index !== 'number' || index < 0 || index >= array.length)
				return console.log('error: index provided is not valid');

			if ( typeof targetIndex !== 'number' || targetIndex < 0 || targetIndex >= array.length)
				return console.log('error: targetIndex provided is not valid');

			var origItem = array[index];

			array.splice(index, 1);

			array.splice(targetIndex, 0, origItem);

			return array;

		},

		trigger : function(name, data) {

			var _data;

			if (!data)
				_data = {};
			else
				_data = data;

			if (!name || typeof name !== 'string')
				return console.log('Error in utils.trigger: No trigger name was provided');

			Rockbase.Globals.els.window.trigger(name, _data);

		},

		getKeyCodes : function(keycode) {
			return {

				BACKSPACE : 8,
				TAB : 9,
				ENTER : 13,
				COMMAND : 15,
				SHIFT : 16,
				CONTROL : 17,
				ALTERNATE : 18,
				PAUSE : 19,
				CAPS_LOCK : 20,
				NUMPAD : 21,
				ESCAPE : 27,
				SPACE : 32,
				PAGE_UP : 33,
				PAGE_DOWN : 34,
				END : 35,
				HOME : 36,

				//arrows
				LEFT : 37,
				UP : 38,
				RIGHT : 39,
				DOWN : 40,

				INSERT : 45,
				DELETE : 46,

				//numbers
				NUMBER_0 : 48,
				NUMBER_1 : 49,
				NUMBER_2 : 50,
				NUMBER_3 : 51,
				NUMBER_4 : 52,
				NUMBER_5 : 53,
				NUMBER_6 : 54,
				NUMBER_7 : 55,
				NUMBER_8 : 56,
				NUMBER_9 : 57,

				//letters
				A : 65,
				B : 66,
				C : 67,
				D : 68,
				E : 69,
				F : 70,
				G : 71,
				H : 72,
				I : 73,
				J : 74,
				K : 75,
				L : 76,
				M : 77,
				N : 78,
				O : 79,
				P : 80,
				Q : 81,
				R : 82,
				S : 83,
				T : 84,
				U : 85,
				V : 86,
				W : 87,
				X : 88,
				Y : 89,
				Z : 90,

				LEFT_WINDOW_KEY : 91,
				RIGHT_WINDOW_KEY : 92,
				SELECT_KEY : 93,

				//number pad
				NUMPAD_0 : 96,
				NUMPAD_1 : 97,
				NUMPAD_2 : 98,
				NUMPAD_3 : 99,
				NUMPAD_4 : 100,
				NUMPAD_5 : 101,
				NUMPAD_6 : 102,
				NUMPAD_7 : 103,
				NUMPAD_8 : 104,
				NUMPAD_9 : 105,
				NUMPAD_MULTIPLY : 106,
				NUMPAD_ADD : 107,
				NUMPAD_ENTER : 108,
				NUMPAD_SUBTRACT : 109,
				NUMPAD_DECIMAL : 110,
				NUMPAD_DIVIDE : 111,

				//function keys
				F1 : 112,
				F2 : 113,
				F3 : 114,
				F4 : 115,
				F5 : 116,
				F6 : 117,
				F7 : 118,
				F8 : 119,
				F9 : 120,
				F10 : 121,
				F11 : 122,
				F12 : 123,
				F13 : 124,
				F14 : 125,
				F15 : 126,

				NUM_LOCK : 144,
				SCROLL_LOCK : 145,

				//punctuation
				SEMICOLON : 186,
				EQUAL : 187,
				COMMA : 188,
				MINUS : 189,
				PERIOD : 190,
				SLASH : 191,
				BACKQUOTE : 192,
				LEFTBRACKET : 219,
				BACKSLASH : 220,
				RIGHTBRACKET : 221,
				QUOTE : 222

			};
			
		}
		
	};
		
})();		