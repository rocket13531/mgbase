//Mgbase

var MG = (typeof MG === 'undefined') ? {} : alert('Error: namespace MG is taken');

//Object to reference in all classes for global triggers/binds
MG.GlobalDispatcher = _.extend({}, Backbone.Events);

MG.Mgbase = Backbone.Model.extend({

	//TODO: LETS ADD AN ATTRIBUTE OF GROUP PARENT TO ALL CHILDREN CONSTRUCTED WITHIN HERE
	
	//CONSTRUCTOR
	constructor: function(attributes, options) {

		//Set _mgbase equal to this for accessibility within functions.
		var _mgbase = this;
		
		//Class constructed state indicates that the class has been successfully constructed.
		_mgbase._constructed = false; 
		
		//Class state is currently active, but beyond destroying, it is not necessarily ready to be used externally.
		_mgbase._active = false; 
		
		//Class is completely loaded and ready to be used.
		_mgbase._ready = false; 

		//Class loader active state
		_mgbase._loaderActive = false;

		//Use loader
		_mgbase._useLoader = true;
		
		//Array of intervals set
		_mgbase._intervals = [];
		
		//Array of timeouts set
		_mgbase._timeouts = [];

		//Default Attributes
		var _mgbaseDefaults = {
			requires: [], //Suggested best practice to include any js dependencies a class might have. (e.g. [MG.Crawler, MG.ClassGroup])
			type: 'mgbase', //Readable classname. Strongly recommend providing type with the same name as the instance name.
			sourceEl: null, //The element representing the class. Optional.
			loaderEl: null, //The element where loader class will be added and removed.
			loaderDelay: 250, //Delay before displaying loader.
			loaderClass: '', //Class type applied to $loaderEl when class loading. Defaults to '[type]_loading'. 
			setReady: true, //If set to false class will not be recognized as ready until manually set with _mgbase._setReady().
			setActive: true, //ADVANCED (recommend leaving as default). If set to false class will not be recognized as active until manually set with _mgbase._setActive().
			debugMode: true, //Show debug messages
			cidAttr: 'data-mgbase-cid' //cid attribute
		};

		if (defaults = _mgbase.defaults) {

			//Merge attributes, into user set defaults, into mgbaseDefaults
		 	_mgbase.attributes = _.extend(_mgbaseDefaults, (typeof _mgbase.defaults === 'function') ? _mgbase.defaults() : _mgbase.defaults, attributes);

    	} else {

			//Merge attributes, into mgbaseDefaults
    		_mgbase.attributes = _.extend(_mgbaseDefaults, attributes);

    	}

		//Listen for errors
		_mgbase.onError();

		//Lets check class dependencies first to see if they all exist.
		if(_mgbase.attributes.requires.length > 0) {

			for(var i = 0; i < _mgbase.attributes.requires.length; i++) {

				var _oObj = _mgbase.attributes.requires[i];

				var _obj = _oObj;

				if(typeof _obj !== 'function' && typeof _obj !== 'object') return _mgbase.logError('Required dependency not found or invalid ['+_oObj+']');

			}

		}

		//If there is a source el attribute set, lets normalize it with jQuery and check to make sure the element exists.
		if(_mgbase.attributes.sourceEl) {
			
			_mgbase.attributes.$sourceEl = $(_mgbase.attributes.sourceEl);
			
			if(_mgbase.attributes.$sourceEl.length !== 1) return _mgbase.logError('Attribute $sourceEl is invalid. Construct class aborted.');
		
		}
		
		//If class loader el provided, lets set it with jQuery. If not, lets see if there is a sourceEl and set it to that. If neither case is true 
		//lets not use a class loader at all.
		if(_mgbase.attributes.loaderEl) {
			
			_mgbase.attributes.$loaderEl = $(_mgbase.attributes.loaderEl)
			
		} else if(_mgbase.attributes.sourceEl) {
			
			_mgbase.attributes.$loaderEl = $(_mgbase.attributes.sourceEl)
			
		} else {

			_mgbase._useLoader = false;

		}

		//If there is a loader, lets add a class to the loader el of loading.
		if(_mgbase._useLoader) {

			if(!_mgbase.attributes.loaderClass) _mgbase.attributes.loaderClass = (_mgbase.attributes.type.toLowerCase()) + '_loading';

			if(typeof _mgbase.attributes.loaderClass !== 'string') return _mgbase.logError('loaderClass type ['+(typeof _mgbase.attributes.loaderClass)+']is not of type string. . Construct class aborted.');

		}
		
		//Call Model Constructor
		
        attributes = _mgbase.attributes;

		Backbone.Model.prototype.constructor.apply(_mgbase, [attributes, options]);

		//Class is now in active state.

		if(_mgbase.attributes.setActive) _mgbase._setActive();		

		//Destroy Handler, deactivate class before proceeding with destroy.
		_mgbase.on('destroy', function() { 
			
			_mgbase.deactivate();
			
		});
		
		//If ready state not being set manually, set it automatically
		if(_mgbase.attributes.setReady) _mgbase._setReady();
		//Else if it is being set manually and useLoader is true, add the class loader to the DOM.
		else if(_mgbase._useLoader) _mgbase._addClassLoader();

		//Listen to all future ready changes
		_mgbase.on('readyChange', function() {

			//If using the class loader proceed
			if(_mgbase._useLoader) {

				//If currently ready remove loader functionality
				if(_mgbase._ready) _mgbase._removeClassLoader();

				else _mgbase._addClassLoader();

			}

		});
	    
		//TODO: Do we need to move this back above setReady logic? The problem is that activate() can
		//now be called before constructed is signaled as true. 

	    //Class has been constructed.

		_mgbase._constructed = true;

		//Call on constructed. Leave blank default for subClass overrides
		_mgbase.onConstructed();

	},

	defaults: function() {
		return {};
	},
	
	//PRIVATE METHODS
	
	_unsetReady: function() {

		var _mgbase = this;
		
		if(!_mgbase._ready) return _mgbase.logWarning('Cannot set _mgbase._ready to false because it already is.');

		if(_mgbase._useLoader) _mgbase.clearTimeout(_mgbase._loadDelayTimer);
		
		_mgbase._ready = false;

		_mgbase.trigger('readyChange', _mgbase);

		return _mgbase;

	},

	_setReady: function() {
		
		var _mgbase = this;
		
		if(_mgbase._ready) return _mgbase.logWarning('Cannot set _mgbase._ready to true because it already is.');

		if(_mgbase._useLoader) _mgbase.clearTimeout(_mgbase._loadDelayTimer);
		
		_mgbase._ready = true;
		
		_mgbase.trigger('readyChange', _mgbase);

		return _mgbase;
		
	},

	_setActive: function() {

		var _mgbase = this;

		_mgbase._active = true;

		return _mgbase;

	},
	
	_addClassLoader: function() {
		
		var _mgbase = this;

		if(!_mgbase._useLoader) return _mgbase.logError('Cannot add class loader because useLoader is set to false.');
		
		if(_mgbase.attributes.$loaderEl.length !== 1) return _mgbase.logError('Cannot add class loader because $classLoader length does not equal 1.');
		
		if(_mgbase.isReady()) return _mgbase.logError('Will not add class loader because the class is ready.')

		_mgbase._loadDelayTimer = _mgbase.setTimeout(function() {

			_mgbase.attributes.$loaderEl.addClass(_mgbase.attributes.loaderClass).addClass('class_loading');

			_mgbase._loaderActive = true;
			
		}, _mgbase.attributes.loaderDelay);

		return _mgbase;
		
	},
	
	_removeClassLoader: function() {

		var _mgbase = this;

		if(!_mgbase._useLoader) return _mgbase.logError('Cannot remove class loader because useLoader is set to false.');

		if(_mgbase.attributes.$loaderEl.length !== 1) return _mgbase.logError('Cannot remove class loader because $classLoader length does not equal 1.');

		if(!_mgbase.isReady) return _mgbase.logError('Cannot remove class loader because the class is not ready yet.')

		_mgbase.attributes.$loaderEl.removeClass(_mgbase.attributes.loaderClass).removeClass('class_loading');

		_mgbase._loaderActive = false;

		return _mgbase;
	
	},
	
	_msgPrefix: function() {
	
		var _mgbase = this;

		return (typeof _mgbase.cid === 'undefined') ? (_mgbase.attributes.type+': ') : _mgbase.attributes.type+'_'+_mgbase.cid+': ';
		
	},
	
	//PUBLIC METHODS
	
	doValidate: function(attr) {

		var _mgbase = this;

		var error = _mgbase.validate(_mgbase.attributes);
	   
	    if (error) {
		
			_mgbase.trigger('error', _mgbase, error);
	    
	    }

	    return _mgbase;

	},

	onError: function() {
		
		var _mgbase = this;
		
		_mgbase.on('error', function(model, error) {
		
			model.logError(error);
			
		});

		return _mgbase;
		
	},

	onConstructed: function() {

	},
	
	//Activate class. This will set active to true and run any custom logic bound to 'activate'
	activate: function(options) {
		
		var _mgbase = this;

		//if(_mgbase._active) return _mgbase.logWarning('Cannot activate class because it is already active.');

		if(!_mgbase._constructed) return _mgbase.logError('Cannot activate class because it has not been constructed.');

		_mgbase.trigger('activate', _mgbase, options);

		_mgbase._setActive();

		

		_mgbase.trigger('activateComplete', _mgbase, options);
		
		//Return object for chaining
		return _mgbase;

	},

	reactivate: function() {

		var _mgbase = this;

		if(!_mgbase._constructed) return _mgbase.logError('Cannot reactivate class because it has not been constructed.');

		if(!_mgbase._active) _mgbase.activate();

		_mgbase.deactivate();

		_mgbase.activate();

		//Return object for chaining
		return _mgbase; 

	},
	
	deactivate: function(options) {
		
		var _mgbase = this;
		
		//if(!_mgbase._active) return _mgbase.logWarning('Cannot deactivate class because it is already deactivated.');

		if(!_mgbase._constructed) return _mgbase.logError('Cannot deactivate class because it has not been constructed.')
	
		_mgbase.trigger('deactivate', _mgbase);

		if(_mgbase._intervals.length > 0) _mgbase.clearAllIntervals();
	
		if(_mgbase._timeouts.length > 0) _mgbase.clearAllTimeouts();
		
		_mgbase._active = false;
		
		_mgbase.trigger('deactivateComplete', _mgbase);

		//Return object for chaining
		return _mgbase;
		
	},
	
	isReady: function() {
		
		return this._ready;
	},
	
	isActive: function() {
		
		return this._active;
		
	},
	
	isLoaderActive: function() {
		
		return this._loaderActive;
		
	},

	isConstructed: function(obj) {

		return (obj instanceof MG.Mgbase);

	},

	getType: function() {

		return this.attributes.type;

	},
	
	alert: function(msg) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		alert(_mgbase._msgPrefix() + msg);

	},

	log: function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		if ( typeof msg === 'object') {

			obj = msg;

			msg = '';

		}

		if ( typeof obj !== 'object')
			obj = '';

		if ( typeof console === 'object' && typeof console.log === 'function') {
			console.log(_mgbase._msgPrefix() + msg, obj);
		}

	},

	logError: function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		if ( typeof msg === 'object') {

			obj = msg;

			msg = '';

		}

		if ( typeof obj !== 'object')
			obj = '';

		if ( typeof console === 'object' && typeof console.log === 'function') {
	
			console.error(_mgbase._msgPrefix() + 'Error - ' + msg, obj);
	
		}

	},

	logWarning: function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		_mgbase.log('Warning - ' + msg, obj);

	},

	logNotice: function(msg, obj) {

		var _mgbase = this;

		if(!_mgbase.attributes.debugMode) return false;

		_mgbase.log('Notice - ' + msg, obj);

	},

	setInterval: function(fn, duration) {

		var _mgbase = this;

		if ( typeof fn !== 'function')
			return _mgbase.logError('setInterval() fn provided is not a function');

		var _duration = duration || 1;

		var interval = setInterval(fn, duration);

		_mgbase._intervals.push(interval);

		return interval;
	},

	clearInterval: function(id) {

		var _mgbase = this;

		window.clearInterval(id);

	},

	clearAllIntervals: function() {

		var _mgbase = this;

		if (_mgbase._intervals.length === 0)
			return false;

		$.each(_mgbase._intervals, function() {
			_mgbase.clearInterval(this);
		});

		_mgbase._intervals = [];

		return true;

	},
	
	setTimeout: function(fn, duration) {

		var _mgbase = this;

		if ( typeof fn !== 'function')
			return _mgbase.logError('setTimeout() fn provided is not a function');

		var _duration = duration || 1;

		var timeout = setTimeout(fn, duration);

		_mgbase._timeouts.push(timeout);

		return timeout;
	},

	clearTimeout: function(id) {

		var _mgbase = this;

		clearTimeout(id);

	},

	clearAllTimeouts: function() {

		var _mgbase = this;

		if (_mgbase._timeouts.length === 0)
			return false;

		$.each(_mgbase._timeouts, function() {

			_mgbase.clearTimeout(this);

		});

		_mgbase._timeouts = [];

		return true;

	}
	
});
