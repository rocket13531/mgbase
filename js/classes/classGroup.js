/*
Class Description: classGroup.js creates a class group which consists of children classes (either more 
class groups with their own children, or just standalone classes). When activate/deactivate is called
on the parent group, it activates/deactivates its children which if that child is a classGroup, will in
turn activate/deactivate its children...and so on and so forth. Additionally, a ClassGroup will not be
indicated as ready until all of its children (and by extension their children) have reported in as ready
as well. ClassGroup.js provides many ways of adding or accessing children classes either via the DOM or
other helper methods. Using classGroup provides the power to write statements such as:
*/

MG.ClassGroup = MG.Mgbase.extend({

	/*
	ClassGroup Object Structure:
	{ class: 'ClassGroup', attributes: {}, options: {} }
	*/

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.ClassGroup.__super__, 'defaults'), {
        	requires: [jQuery, MG.ClassCrawler], //This class depends on (in addition to other things) MG.ClassCrawler
        	classCrawler: MG.classCrawler, //ClassCrawler class instance
        	activate: true, //Auto activate - true/false - Remember this will activate all children objects of this class
        	type: 'ClassGroup', //IMPORTANT - do not touch - must match class name (ClassGroup)
        	groupType: 'ClassGroup', //Type of group (e.g. Carousel)
            parentActivated: true, //Can be activated by parents
            parentDeactivated: true, //Can be deactivated by the parent
			crawlSelf: true, //Crawl sourceEl
			addChildren: [], //Class Objects of children to add on construct.
			addChildrenByEl: {
				el: '', //el(s) to find children at. Will override crawlself which would crawl from sourceEl
				crawl: false, //will crawl each matched el for Classes
				filterParents: '', //Parents to filter results by
				filter: '', //Els to filter
				andSelf: true,
				filterGroupTypes: [] //Class groupTypes to filter
			},
			groupParent: null,
			groupInvokeAttr: 'data-mgbase-ginvoke',
			gidAttr: 'data-mgbase-group-gid', //gidAttr is the attribute for finding group ids (gid).
										//group classes (classes created by groups) use an id of gid.
			idPrefix: 'g', //Prefix for group id
			setActive: false, //ADVANCED - LEAVE AS FALSE - this will stop the active state from defaulting to true so we can call activate later
			_children: [/* class: 'ClassGroup', attributes: {}, options: {} */], //array of children class OBJECTS (merged from addChildren & addChildrenByEl)
			_childrenConstructed: [], //array of children class INSTANCES (children that have been constructed)
			_readyInterval: null //Interval to check if children are ready
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
		if(_.has(attrs, 'addChildrenByEl')) {
			if(attrs.addChildrenByEl.el) { 
				if(attrs.addChildrenByEl.el.length < 1) return 'addChildrenByEl.el is invalid';
			}
		}

		if(_.has(attrs, 'classCrawler')) if(typeof attrs.classCrawler !== 'object' || !attrs.classCrawler._constructed) return 'classCrawler is not valid';
	
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();

		if(_mgbase.attributes.addChildrenByEl.el) {

			_mgbase.addChildrenByEl(_mgbase.attributes.addChildrenByEl.el, {
				crawl: _mgbase.attributes.addChildrenByEl.crawl,
				filterParents: _mgbase.attributes.addChildrenByEl.filterParents, 
				filter: _mgbase.attributes.addChildrenByEl.filter,
				filterGroupTypes: _mgbase.attributes.addChildrenByEl.filterGroupTypes,
				andSelf: _mgbase.attributes.addChildrenByEl.andSelf
			});
		}

		if(_mgbase.attributes.addChildren.length > 0) _mgbase.addChildren(_mgbase.attributes.addChildren);

		//TODO: Build in group loader

		_mgbase.log('Group Constructed!');

	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

		_mgbase.attributes.gid = _.uniqueId('g');

		if(_mgbase.attributes.$sourceEl) {
			if(_mgbase.attributes.$sourceEl.length === 1) _mgbase.attributes.$sourceEl.attr(_mgbase.attributes.gidAttr, _mgbase.attributes.gid);
		}

		_mgbase.attributes.groupType = _mgbase.attributes.groupType;

		//If crawlSelf true lets first see if addChildrenByEl.el exists and use that, if not, then lets crawl from the sourceEl.
		if(_mgbase.attributes.crawlSelf) {

			_mgbase.attributes.addChildrenByEl.el = (_mgbase.attributes.addChildrenByEl.el) ? $(_mgbase.attributes.addChildrenByEl.el) : _mgbase.attributes.$sourceEl;

		}

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

		_mgbase.attributes.$sourceEl.append('<p>'+_mgbase.attributes.groupType+'</p>');

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	//TODO: Investigate - Is this being done somewhere where it doesn't belong? Should this be here?
	_activateChild: function(obj) {

	},

	//TODO: Investigate - Is this being done somewhere where it doesn't belong? Should this be here?
	_activateChildren: function(obj) {

	},

	//Obj can be either a class instance object, or a classData object
	_addChild: function(obj) { 

		var _mgbase = this;

		var _obj = obj;

		//Lets set some defaults to the child object so that we can abstract data from a 'dataclass object' later when
		//deciding whether or not to construct a standalone class (as opposed to a classGroup).
		if(_obj.class !== _mgbase.attributes.type) {

			_mgbase.log('In? ', _mgbase)

			_obj.attributes = _.extend({
				groupParent: _mgbase,
				parentActivated: true, 
				parentDeactivated: true,
				gid: _.uniqueId(_mgbase.attributes.idPrefix)
			}, obj.attributes);

			var $sourceEl = $(_obj.attributes.sourceEl);

			if($sourceEl.length === 1) $sourceEl.attr(_mgbase.attributes.classCrawler.attributes.cidAttr, _obj.attributes.gid);

		} else if(_obj.class === _mgbase.attributes.type) {

			_obj.attributes = _.extend({
				activate: _mgbase._active, 
				groupParent: _mgbase
			}, _obj.attributes);

		}

		//We must always construct groupTypes of this.groupType (aka 'ClassGroup') so lets do that now.
		//If the obj is already a baseclass instance then skip ahead...its already been constructed
		if(_obj.class === _mgbase.attributes.type && !(_obj instanceof MG.Mgbase)) {
			_obj = _mgbase.constructClassData(_obj);
		}

		_mgbase.attributes._children.push(_obj);

		_mgbase.set('_children', _mgbase.attributes._children);

		return _obj;

	},

	//Objs can be either class instance objects, or classData objects
	_addChildren: function(objs, options) {

		var _mgbase = this;

		var _children = [];

		var _options = _.extend({filterGroupTypes: []}, options);

		//underscore won't seem to iterate each as expected
		for(var j = 0; j < objs.length; j++) {

			var _obj = objs[j];

			var _filter = false;

			//If there are filter instructions lets see if this object
			//should be filtered and not added to the children set.
			if(_options.filterGroupTypes && _obj.attributes.groupType) {

				for(var i = 0; i < _options.filterGroupTypes.length; i++) {

					if(_obj.attributes.groupType == _options.filterGroupTypes[i]) _filter = true;

					if(_filter) break;

				}

			}

			if(!_filter) _children.push(_mgbase._addChild(_obj));

		};

		//Return the array of children objects added
		return _children;

	},

	_constructClassData: function(classObj) {

		var _mgbase = this;

		//Lets construct the class from the class obj format
		//return new MG[classObj.class](classObj.attributes, classObj.options);

		return new MG[classObj.class](classObj.attributes, classObj.options);

		if(_mgbase.getChildByGid(classObj.attributes.gid)) return new MG[classObj.class](classObj.attributes, classObj.options);

		else {
			_mgbase.logWarning('Unable to construct classObj because it has not yet been added as a child. Adding child and calling construct again.');
			_mgbase.addChildren(classObj);
		}

		//TODO: check if a child and if so, construct. If not, we need to add the child
		/*if(_mgbase.getChildren({gid: classObj.attributes.gid})) return new MG[classObj.class](classObj.attributes, classObj.options);
		else {
			
			_mgbase.logWarning('Unable to construct classObj because it has not yet been added as a child. Adding child and calling construct again.');

			_mgbase.addChildren(classObj);

		}*/

	},

	//Get Class Id's by type and group type
	_getGidsByGroupTypes: function(groupTypes) {

		var _mgbase = this;

		var _gids = [];

		if(!groupTypes) return _mgbase.logError('types and groupTypes provided to _getGidsByTypes are invalid.');

		var _children = _.clone(_mgbase.attributes._children);

		var _child = null;

		for(var i = 0; i < _children.length; i++) {

			_child = _children[i];

			if(_child.attributes.groupType !== 'undefined') {

				if(_.contains(groupTypes, _child.attributes.groupType)) {
					_gids.push(_child.attributes.gid);
				}
			}

		}

		//TODO: is _.unique() necessary?
		return _.unique(_gids);

	},

	//Get Class Id's by jquery selector
	_getGidsByEl: function(el) {

		var _mgbase = this;

		var $el = $(el);

		if($el.length < 1) return _mgbase.logError('el provided to _getGidByEl is invalid');

		return _.union(
			_mgbase.attributes.classCrawler.getCids($el),
			_mgbase.attributes.classCrawler.getCids($el, { cidAttr: _mgbase.attributes.gidAttr }) 
		);

	},

	//Override the msgPrefix of baseclass
	_msgPrefix: function() {
	
		var _mgbase = this;

		return (typeof _mgbase.cid === 'undefined') ? (_mgbase.attributes.groupType+': ') : _mgbase.attributes.groupType+'_'+_mgbase.cid+': ';
		
	},

	_childrenReady: function() {

		var _mgbase = this;

		var _allReady = true;

		var _child = null;

		for(var i = 0; i < _mgbase.attributes._children.length; i++) {

			_child = _mgbase.attributes._children[i];

			if(_mgbase.getObjType(_child) == 'instance') {

				if(!_child.isReady()) {
					_allReady = false;
					break;
				}

			}

		}

		return _allReady;

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate lets activate all children of the classGroup.
				$.each(_mgbase.attributes._children, function(i) {

					var _obj = this;

					if(_obj.attributes.parentActivated) {

						var _objType = _mgbase.getObjType(_obj);

						if(_objType === 'instance' && !_obj._active) _obj.activate();

						else if(_objType === 'classdata') {
							_mgbase.attributes._children[i] = _mgbase._constructClassData(_obj);

						}

					} 

				});

				//TODO: Remove, just for testing purposes
				if(_mgbase.attributes.$sourceEl.length == 1) _mgbase.attributes.$sourceEl.css({background: 'blue'});

				//Logic for classGroup ready handling...
				//If children are not ready
				if(!_mgbase._childrenReady()) {
					
					//If children are not ready and this ready state is true, then it is incorrect. Set to false.
					if(_mgbase.isReady()) _mgbase._unsetReady();

					//Set interval to check when children are ready
					_mgbase.set('_readyInterval', _mgbase.setInterval(function() {	

						//If children are ready clear the readyInterval checker and set the class as ready.
						if(_mgbase._childrenReady()) {

							_mgbase.clearInterval(_mgbase.attributes._readyInterval);

							return _mgbase._setReady();

						}

					}, 60));
			
				//If the children are ready but this class isn't set as ready
				//set it as ready now because it must be.
				} else if(!_mgbase.isReady()) {

					_mgbase._setReady();

				}

				//_mgbase._checkChildrenReady();

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				//On deactivate lets deactivate all children of the classGroup
				$.each(_mgbase.attributes._children, function(i) {

					var _obj = this;

					if(_obj.attributes.parentDeactivated) {

						if(_mgbase.getObjType(_obj) === 'instance') _obj.deactivate();

					} 

				});

				_mgbase.log("Goodnight.");

				if(_mgbase.attributes.$sourceEl.length == 1) _mgbase.attributes.$sourceEl.css({background: '#fff'});

			});
			
		}

	},

	onConstructed: function() {

		var _mgbase = this;

		if(_mgbase.attributes.activate) _mgbase.activate();
		

	},

	//TODO: This cannot be public if classObj isn't required to have a class id. INVESTIGATE.
	constructClassData: function(classObj) { 

		var _mgbase = this;

		return _mgbase._constructClassData(classObj);

	},

	//TODO: We need an addChild option
	addChild: function(classObj, options) {

		var _mgbase = this;

		if(classObj.length > 1) return _mgbase.log('classObj passed to addChild is invalid.');

		return _mgbase.addChildren([classObj], options)[0];

	},

	//Add Children
	//TODO: Make sure this accepts instantiated objects
	addChildren: function(classObjs, options) {

		var _mgbase = this;

		var _options = _.extend({filterGroupTypes: '', activate: ''}, options);

		if(!classObjs) return _mgbase.logError('classObj passed to addChildren is invalid.');

		var _children = _mgbase._addChildren(classObjs, _options);

		return _children;

		//if(classObj.groupType === _mgbase.groupType) _constructClassData(obj);

		//...

	},

	addChildrenByEl: function(el, options) {

		var _mgbase = this;

		var _options = _.extend({
			crawl: false, //will crawl each matched el for Classes
			filterParents: '', //Parents to filter results by
			filter: '', //Els to filter
			andSelf: true,
			filterGroupTypes: [] //Class groupTypes to filter
		}, options);

		$el = $(el);

		var _classObjs = [];

		if($el.length < 1) return _mgbase.logError('el provided to addChildrenByEl is invalid');

		//Get site crawl for group class objects
		var _classObjs = _mgbase.attributes.classCrawler.extract($el, {
			invokeAttr: _mgbase.attributes.groupInvokeAttr, 
			cidAttr: _mgbase.attributes.gidAttr,
			crawl: _options.crawl, 
			filterParents: _options.filterParents, 
			filter: _options.filter, 
			andSelf: _options.andSelf
		});

		//Merge site crawl for group class objects with site crawl for class objects
		_classObjs = _.union(_classObjs, _mgbase.attributes.classCrawler.extract($el, {
				crawl: _options.crawl, 
				filterParents: _options.filterParents, 
				filter: _options.filter, 
				andSelf: _options.andSelf
			})
		);

		return _mgbase.addChildren(_classObjs, {filterGroupTypes: _options.filterGroupTypes});

	},

	/*TODO: Should have a get child method, which if more than one will return getChildren*/

	getChildByEl: function(el, options) {

		var _mgbase = this;

		var $el = $(el);

		if($el.length !== 1) return _mgbase.logError('el passed to getChildByEl is invalid.');

		return _mgbase.getChildren(_.extend({}, options, {els: $el}))[0];

	},

	getChildByGroupType: function(groupType, options) {

		var _mgbase = this;

		if(typeof groupType !== 'string') return _mgbase.logError('groupType passed to getChildByGroupType is invalid.');

		return _mgbase.getChildren(_.extend({}, options, {groupTypes: groupType}))[0];

	},

	//TODO: Change CID to GID
	getChildByGid: function(gid, options) {
		
		var _mgbase = this;

		if(typeof gid !== 'string') return _mgbase.logError('gid passed to getChildByGid is invalid.');

		return _mgbase.getChildren(_.extend({}, options, {gids: gid}))[0];

	},

	getChildByIndex: function(index, options) {

		var _mgbase = this;

		if(index < 0 || typeof index !== 'number') return _mgbase.logError('index passed to index is invalid.');

		return _mgbase.getChildren(_.extend({}, options, {index: index}))[0];

	},

	getChild: function(options) {

		var _mgbase = this;

		if(!options) _mgbase.logError('options passed to getChild are invalid.');

		var _options = _.extend({index: null, el: '', groupType: '', gid: '', andDescendants: true, autoConstruct: true}, options);

		var _child = _mgbase.getChildren(_.extend({
			index: _options.index, 
			els: _options.el, 
			groupTypes: _options.groupType, 
			gids: _options.gid,
			andDescendants: _options.andDescendants,
			autoConstruct: _options.autoConstruct
		}));

		if(_child.length > 1) return _mgbase.logError('Unexpected result in getChild. Only one object expected.');

		//TODO: ACTIVATES ALL MATCHES
		return _child[0];

	},

	//TODO: Should have a filter option for only instances

	//TODO: IMPORTANT - Fix getChildByIndex
	getChildren: function(options) {

		var _mgbase = this;

		if(_mgbase.attributes._children.length < 1) return [];

		//TODO: passing nothing into options should return all children, including andDescendants.

		//TODO: get rid of plurals in options. 

		//TODO: groupTypes - convert to groupType and convert to space delimited string
		//E.g. groupType: 'DemoOverlay_1 DemoOverlay_2'
		//E.g. gid: 'c12 c34 c35' 			32
		var _options = _.extend({index: null, els: '', groupTypes: [], gids: [], andDescendants: true, autoConstruct: true}, options);

		//Create arrays from string for options that accept string types
		if(typeof _options.groupTypes == 'string') _options.groupTypes = _options.groupTypes.split(' ');
		
		if(typeof _options.gids == 'string') _options.gids = _options.gids.split(' ');

		//Error Check
		if(typeof _options.groupTypes !== 'object') return _mgbase.logError('groupTypes provided to getChildren must be an object.');

		if(typeof _options.gids !== 'object') return _mgbase.logError('gids provided to getChildren must be an object.');

		//Set vars
		var _children = [];

		var _gids = _options.gids;

		var _indexGids = _options.index;

		var _elGids = [];

		var _groupTypeGids = [];

		var _ret = [];

		var _filter = false;

		_children = _.clone(_mgbase.attributes._children);

		//If els, get gids by el.
		if(_options.els) {
			_elGids = _mgbase._getGidsByEl(_options.els);
			_filter = true;
		}

		//If groupTypes, get gids by group type
		if(_options.groupTypes.length > 0) {
			_groupTypeGids = _mgbase._getGidsByGroupTypes(_options.groupTypes);
			_filter = true;
		}

		//If gids passed in, set our _gids to those.
		if(_options.gids.length > 0) {
			_gids = _options.gids;
			_filter = true;
		}

		if(typeof _options.index == 'number' && _options.index > -1) {
			_indexGids = _children[_options.index].attributes.gid;
			_filter = true;
		}

		//TODO: Very inefficient I believe...at least in terms of duplicate logic. Also, recursive descendants search
		//not only duplicate logic but might not catch duplicate class objects. This needs some serious attention.
		//If no search options were set, then the user is asking for all children so return all children.
		if(!_filter) {

			_ret = _children;

			//TODO: Duplicate logic as below. Anyway to abstract this logic?
			if(_options.autoConstruct) {

				var _child;
				
				for(var j = 0; j < _children.length; j++) {

					_child = _children[j];

					if(_mgbase.getObjType(_child) == 'classdata') {
						_ret[j] = _mgbase.attributes._children[j] = _mgbase.constructClassData(_child);
						
					}

					if(_options.andDescendants && _child.attributes.type == _mgbase.attributes.type) {

						_ret = _.union(_ret, _mgbase.attributes._children[j].getChildren({andDescendants: true}) );

					}
					

				}
			}

		}
		//If filters set, combine the results of all and find if they match any children objects
		else {

			_gids = _.union(_gids, _elGids, _groupTypeGids, _indexGids);

			if(_gids.length > 0) {

				var _child;
			
				for(var i = 0; i < _children.length; i++) {

					_child = _children[i];

					//If this child's gid is in the list of _gids found, lets add it to our return;
					if(_.contains(_gids, _child.attributes.gid)) {

						//If autoConstruct is true & the child's object type is 'classdata' then construct 
						//the class now and wipe the existing data object with the returned instance from constructClassData.
						if(_options.autoConstruct && _mgbase.getObjType(_child) == 'classdata') {
							
							//Update this classes child with the constructed instance object, and update the
							//child returned with that same instance reference.
							_child = _mgbase.attributes._children[i] = _mgbase.constructClassData(_child);
						
						}
						
						_ret.push(_child);

					}

					//If andDescendants set true, lets check to see if this child's children has any matches,
					//and of course they will check their children and so on and so forth
					if(_options.andDescendants) {
						
						if(_child.attributes.type == _mgbase.attributes.type) {

							//Capture results of descendants. 
							//We do not want to crawl the els again so override with a blank els property.
							//We also do not want to check index passed in against descendants (only against direct children) so set that to null.
							//Extend the options.gids with the elGids results so that the element gids results are checked against all descendent
							//Children's gids
							if(_options.groupTypes.length > 0 || _options.gids.length > 0) {

								var _desc = _child.getChildren(_.extend(options, {els: '', index: null, gids: _.extend(_options.gids, _elGids) }));
							
								$.each(_desc, function() {

									_ret.push(this);

								});

							}

						}

					}

				}

			}

		}

		return _ret;

	},

	//Returns instance, classdata, or undefined. If an object is an instance
	getObjType: function(obj) {

		var _mgbase = this;

		//If instance of MG.Mgabse we'll indicate it as an instance
		if(obj instanceof MG.Mgbase) return 'instance';

		//Duck typing - If the object contains a class and options, and is not an instance, we'll ducktype it as classdata
		else if(typeof obj.class == 'string' && typeof obj.options == 'object') return 'classdata';

		else return 'undefined';

	}

});