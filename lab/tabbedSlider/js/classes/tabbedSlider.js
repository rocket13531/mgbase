MG.TabbedSlider = MG.Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.TabbedSlider.__super__, 'defaults'), {
            type: 'TabbedSlider',
			initIndex: 0,
			slideViewport: '#tabbed-slider',
			slideHandle: '#tabbed-slider .tabbed-slider-handle',
			slideItems: '#tabbed-slider .tabbed-slider-item',
			navItems: null,
			transitionSlide: null,
			slideDuration: 1200,
			slideEasing: 'swing',
			showViewportDuration: 600,
			nextButton: null,
			prevButton: null,
			activeClass: 'active-device',
			bindKeys: true,
			showHideArrows: false,
			onLoadComplete: function() {},
			onSlideComplete: function() {},
			_currentIndex: 0,
			_sliding: false,
			_slideHandleHeight: 0,
			_slideViewportWidth: 0,
			_slideItemTransWidth: 0, //Item Width + Transition Item Width
			_numSlideItems: 0,
			_initiated: false
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
		var _mgbase = this;
	
		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';
		
		if(_.has(attrs, '_numSlideItems')) if(attrs._numSlideItems < 1) return 'Num slide items is invalid. There must be at least 1 or more';
			
		if(_.has(attrs, 'navItems') && _.has(attrs, '_numSlideItems')) {
			
			if(attrs.navItems) {
				if(attrs.navItems.length !== attrs._numSlideItems) return 'The number of nav items does not match the number of slide items';
			}
			
		}
		
		if(_.has(attrs, 'nextButton')) {

			if(attrs.nextButton) if(attrs.nextButton.length !== 1) return 'nextButton is invalid.';
			
		}
		
		if(_.has(attrs, 'prevButton')) {
			
			if(attrs.prevButton) if(attrs.prevButton.length !== 1) return 'prevButton is invalid.';
			
		}
		
		if(_.has(attrs, '_currentSlide')) if(attrs._currentSlide >= attrs._numSlideItems || attrs._currentSlide < 0) return '_currentSlide ['+attrs._currentSlide+'] is invalid';
			
	},

	//The starting place of all classes
	initialize: function() {
		
		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();
		
		_mgbase.slideTo(_mgbase.attributes.initIndex, {snapIt: true});

		$(_mgbase.attributes.slideItems[_mgbase.attributes.initIndex]).show();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;
		
		//Normalize Properties as $uery objects
		
		_mgbase.attributes.slideViewport = $(_mgbase.attributes.slideViewport);
	
		_mgbase.attributes.slideHandle = $(_mgbase.attributes.slideHandle);
	
		_mgbase.attributes.slideItems = $(_mgbase.attributes.slideItems);
	
		_mgbase.attributes.transitionSlide = $(_mgbase.attributes.transitionSlide);
		
		if(_mgbase.attributes.nextButton) _mgbase.attributes.nextButton = $(_mgbase.attributes.nextButton);
		
		if(_mgbase.attributes.prevButton) _mgbase.attributes.prevButton = $(_mgbase.attributes.prevButton);
		
		if(_mgbase.attributes.navItems) _mgbase.attributes.navItems = $(_mgbase.attributes.navItems);
	
		//Config Globals
		var handleWidth = 0;
		
		_mgbase.attributes.slideHandle.attr('style', 'inherit');
		
		_mgbase.attributes._slideViewportWidth = _mgbase.attributes.slideViewport.outerWidth();
	
		_mgbase.attributes._slideHandleHeight = _mgbase.attributes.slideHandle.height();
	
		_mgbase.attributes._numSlideItems = _mgbase.attributes.slideItems.length;
		
		if(_mgbase.attributes.initIndex < 0 || _mgbase.attributes.initIndex > _mgbase.attributes._numSlideItems - 1) _mgbase.attributes.initIndex = 0;

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;
		
		if(_mgbase.attributes.nextButton) _mgbase.attributes.nextButton.show();

		if(_mgbase.attributes.prevButton) _mgbase.attributes.prevButton.show();

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);
		
		if(_mgbase.attributes.navItems) _mgbase.binds.setNavItemBinds(_mgbase);
		
		if(_mgbase.attributes.nextButton) _mgbase.binds.setNextBind(_mgbase);
		
		if(_mgbase.attributes.prevButton) _mgbase.binds.setPrevBind(_mgbase);
		
		if(_mgbase.attributes.bindKeys) _mgbase.binds.setBindKeys(_mgbase);

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		},
		
		setNavItemBinds: function(context) {
			
			var _mgbase = context;
			
			$.each(_mgbase.attributes.navItems, function(i) {
			
				$this = $(this);
				
				$this.on('click tap', function(e) {
				
					e.preventDefault();
						
					_mgbase.slideTo(i);
					
				});
				
			});
			
		},
		
		setBindKeys: function(context) {
			
			var _mgbase = context;
			
			if(_mgbase.attributes.bindKeys) {

				$(window).on('keydown', function(e) {
					
					if(_mgbase.attributes._alive) {

						//If left
						if(e.which === 37) _mgbase.slideNext();
						//If right
						if(e.which === 39) _mgbase.slidePrev();

					}

				});

			}
			
		},
		
		setNextBind: function(context) {
			
			var _mgbase = context;
			
			_mgbase.attributes.nextButton.on('click tap', function(e) {
				
				e.preventDefault();
				
				_mgbase.slideNext();
				
			});
			
		},
		
		setPrevBind: function(context) {
			
			var _mgbase = context;
			
			_mgbase.attributes.prevButton.on('click tap', function(e) {
				
				e.preventDefault();
				
				_mgbase.slidePrev();
				
			});
			
		}

	},
	
	_prepareForSlide: function(targetIndex, position) {
		
		var _mgbase = this;
		
		var $targetSlide = $(_mgbase.attributes.slideItems[targetIndex]);
		
		var targetSlidePos = (position < 0) ? 0 : _mgbase._getSlideItemTransWidth();
		
		if(position < 0) {
	
			targetSlidePos = 0;
	
		} else {
	
			targetSlidePos = _mgbase._getSlideItemTransWidth();
	
		}
	
		$targetSlide.css({left: targetSlidePos }).show();
		
	},
	
	_shiftSlideHandle: function(handleDirection) {
		
		var _mgbase = this;
	
		var $currentSlide = $(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]);
		
		var offset = $currentSlide.offset();
		
		var handlePosition = (handleDirection < 0) ? _mgbase._getSlideItemTransWidth() * -1 : 0;
		
		$currentSlide.css({position: 'absolute', left: offset.x, top: offset.y});
		
		_mgbase.attributes.slideHandle.css({left: handlePosition });
		
		$currentSlide.css({position: 'absolute', left: handlePosition * -1, top: 0});
		
	},
	
	_slideComplete: function(index, options) {
		
		var _mgbase = this;
		
		var _options = $.extend({instantLoad: false}, options);
		
		$(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]).removeClass('circular-slider-active-item');
		
		$(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]).hide().css({position: 'absolute', left: 0});
		
		_mgbase.set('_currentIndex', index);
		
		_mgbase._shiftSlideHandle(1);
		
		_mgbase.set('_sliding', false);
		
		$(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]).addClass('circular-slider-active-item');
		
		if(_mgbase.attributes.showHideArrows) {
			
			if(_mgbase.attributes._currentIndex === _mgbase.attributes._numSlideItems - 1) _mgbase.attributes.nextButton.hide();
			
			else _mgbase.attributes.nextButton.show();
			
			if(_mgbase.attributes._currentIndex === 0) _mgbase.prevButton.hide();
			
			else _mgbase.attributes.prevButton.show();
			
		}
		
		MG.GlobalDispatcher.trigger('SlideCompleteEvent', {
			index: _mgbase.attributes._currentIndex, 
			el: $(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]),
			instantLoad: options.instantLoad
		});

		_mgbase.attributes.onSlideComplete();
		
	},
	
	_getSlideItemWidth: function() {
		
		var _mgbase = this;
	
		return $(_mgbase.attributes.slideItems[_mgbase.attributes._currentIndex]).outerWidth();
		
	},
	
	_getSlideItemTransWidth: function() {
		
		var _mgbase = this;
	
		return _mgbase._getSlideItemWidth();
		
	},
	
	_showViewport: function() {
		
		var _mgbase = this;
		
		if(_mgbase.attributes.showViewportDuration > 0) _mgbase.attributes.slideViewport.animate({opacity: 1}, _mgbase.attributes.showViewportDuration);
		
		else _mgbase.attributes.slideViewport.css({opacity: 1});
		
	},
	
	_setNavActive: function(index) {
	
		var _mgbase = this;
		
		if(!_mgbase.attributes.navItems) return;
	
		var $el = $(_mgbase.attributes.navItems[index]);
		
		_mgbase._removeNavActive();
	
		if(!$el.hasClass(_mgbase.attributes.activeClass)) $el.addClass(_mgbase.attributes.activeClass);
		
	},
	
	_removeNavActive: function() {
		
		if(!_mgbase.attributes.navItems) return;
	
		$(_mgbase.attributes.navItems[_mgbase.attributes._currentIndex]).removeClass(_mgbase.attributes.activeClass);
		
	},
	
	//PUBLIC METHODS
	
	slideTo: function(index, options) {
		
		var _mgbase = this;
		
		var oIndex, oOptions;
		
		oIndex = index;
		
		oOptions = options;
		
		if(_mgbase.attributes._sliding) return false;
		
		if(typeof index !== 'number') return _mgbase.logError('index is not valid')
		
		var _detectDirection = true;
		
		var _dirSign = 1;
		
		if(index < 0) { index = _mgbase.attributes._numSlideItems - 1; _dirSign = -1; }
		
		if(index > _mgbase.attributes._numSlideItems - 1) { index = 0; _dirSign = -1; }
		
		var _options = $.extend({instantLoad: false, direction: 1, snapIt: false, triggerData: {snapIt: false}}, options);
		
		if(index >= _mgbase.attributes._currentIndex) _options.direction = 1 * _dirSign;
		
		else if(index < _mgbase.attributes._currentIndex) _options.direction = -1 * _dirSign;
		
		var _animateToPos = null;
		
		if(!_mgbase.attributes._instantiated) _mgbase.set('_instantiated', true);
		
		_mgbase.set('_sliding', true);
		
		_mgbase._setNavActive(index);
		
		//TODO: FIX TRIGGER MECHANISMS - USE GLOBAL DISPATCHER
		MG.GlobalDispatcher.trigger('SlideStartEvent', {
			
			oIndex: _mgbase.attributes._currentIndex,
	
			index: index,
			
			el: $(_mgbase.attributes.slideItems[index]),
	
			snapIt: _options.snapIt,
			
			instance: _mgbase,
			
			instantLoad: _options.instantLoad
	
		});

		//Config Slider
		if(_options.direction < 0) {
	
			_mgbase._shiftSlideHandle(_options.direction);
	
			_mgbase._prepareForSlide(index, -1);
	
			_animateToPos = 0;
	
		} else {
	
			_mgbase._prepareForSlide(index, 1);
	
			_animateToPos = -1 * (_mgbase._getSlideItemWidth());
	
		}
		
		//Slide it
		if(_options.snapIt) {

			_mgbase.attributes.slideHandle.css({left: _animateToPos});

			_mgbase._slideComplete(index, _options);

		} else {

			_mgbase.attributes.slideHandle.animate({left: _animateToPos}, _mgbase.attributes.slideDuration, function() {

				_mgbase._slideComplete(index, _options);

			});

		}
		
		return true;
		
	},
	
	getNavItem: function(index) {
		
		var _mgbase = this;
		
		return $(_mgbase.attributes.navItems[index]);
		
	},
	
	slideNext: function(options) {
		
		var _mgbase = this;

		_mgbase.slideTo(_mgbase.attributes._currentIndex + 1);

	},
	
	slidePrev: function(options) {
		
		var _mgbase = this;

		_mgbase.slideTo(_mgbase.attributes._currentIndex - 1, {direction: -1});

	},
	
	getCurrentIndex: function() {
		
		var _mgbase = this;
	
		return _mgbase.attributes._currentIndex;
		
	},
	
	isSliding: function() {
		
		var _mgbase = this;

		return _mgbase.attributes._sliding;

	}
	
});







