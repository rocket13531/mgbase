MG.ClassCrawler = MG.Mgbase.extend({

	//Default attributes
	defaults: function() {

        return _.extend(_.result(MG.ClassCrawler.__super__, 'defaults'), {
            type: 'ClassCrawler',
            invokeAttr: 'data-mgbase-invoke',
            cidAttr: 'data-mgbase-class-gid'
        });

    },

    //Attribute validations
	validate: function(attrs, options) {
	
	},

	//The starting place of all classes
	initialize: function() {

		var _mgbase = this;

		//Configure Attribute values
		_mgbase._config();

		//Validate Attribute values
		_mgbase.doValidate();

		//Setup DOM
		_mgbase._setup();

		//Set event binds
		_mgbase._setBinds();
		
	},
	
	//Configure attributes and global values
	_config: function() {

		var _mgbase = this;

		//Create new jQuery find method to include self in selection
		if(!$.fn.findAndSelf) $.fn.findAndSelf = function(selector) { return this.find(selector).andSelf().filter(selector); }

	},

	//Setup Dom/Page based on configured attributes and values
	_setup: function() {

		var _mgbase = this;

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	_getCid: function(el, options) {

		var _mgbase = this;

		var $el = $(el);

		if($el.length !== 1) return _mgbase.logError('el in _getCid is invalid.');

		var _options = $.extend({
			cidAttr: _mgbase.attributes.cidAttr
		}, options);

		return $el.attr(cidAttr);

	},

	//PUBLIC METHODS
	
	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('activate', function(e, data) {

				//On activate
				_mgbase.log('Add reactivation logic here.');

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			_mgbase.on('deactivate', function(e, data) {

				_mgbase.log('Add deactivation logic here.');

			});
			
		}

	},

	getCids: function(el, options) {

		var _mgbase = this;

		var $el = $(el);

		if($el.length < 1) return _mgbase.logError('el in getCid is invalid.');

		var _options = _.extend({
			cidAttr: _mgbase.attributes.cidAttr
		}, options);

		var _cids = [];

		$.each($el, function() {

			var $this = $(this);

			var _cid = $this.attr(_options.cidAttr);

			if(_cid) _cids.push(_cid);

		});

		return _cids;

	},

	//Extract class invoke data
	extract: function(el, options) {

		var _mgbase = this;

		var _options = $.extend({
			crawl: true,
			filterParents: '', 
			filter: '',
			andSelf: true,
			invokeAttr: _mgbase.attributes.invokeAttr,
			cidAttr: _mgbase.attributes.cidAttr
		}, options);

		var $el = $(el);

		var $classes = null;

		var _classes = [];

		if($el.length < 1) return _mgbase.logError('El provided to extract is invalid.');

		$classes = (_options.andSelf) ? $el.findAndSelf('['+_options.invokeAttr+']').not(_options.filter) : $el.find('['+_options.invokeAttr+']').not(_options.filter);

		$classes = $classes || null;

		//If we want to exclude classes with a particular parent.
		if(_options.filterParents) $classes = $classes.filter(function() { return $(this).parents(_options.filterParents).length < 1 });

		if(!$classes) {
		
			return _classes;
		
		}

		else {

			$.each($classes, function() {

				var $this = $(this);

				if($this.attr(_options.cidAttr)) return _mgbase.log('El has already been constructed - '+_options.cidAttr);

				var _invokeData = $.parseJSON($this.attr(_options.invokeAttr));

				if(!_invokeData) return _mgbase.logError('invoke attributes not found in extract in el - ', $this);

				var _class = _invokeData.class;

				var _attributes = _.extend({sourceEl: $this}, _invokeData.attributes);

				var __options = _.extend({}, _invokeData.options);

				if(!_class) return _mgbase.logError('class not found in extract in el - ', $this);

				_classes.push({class: _class, attributes: _attributes, options: __options});

			});

		}

		return _classes;

	}

});