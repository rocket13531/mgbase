//Class Description: Slider.js serves as solution for horizontal & vertical circular/non-circular carousels, as well as
//Tabbed browsing (or a fade gallery effect). Slider.js encapsulates bxslider.js within the mgbase class structure in order
//to standardize its usage and behaviors along with all other mgbase classes.

MG.Slider = MG.Mgbase.extend({
	
	defaults: function() {

        return _.extend(_.result(MG.Slider.__super__, 'defaults'), 
        {
        	requires: [$.fn.bxSlider],
            type: 'Slider',
			context: null,
			viewport: '.slider',

			// BXSLIDER - GENERAL
			mode: 'horizontal',
			slideSelector: '',
			infiniteLoop: true,
			hideControlOnEnd: false,
			speed: 500,
			easing: null,
			slideMargin: 0,
			startSlide: 0,
			randomStart: false,
			captions: false,
			ticker: false,
			tickerHover: false,
			adaptiveHeight: false,
			adaptiveHeightSpeed: 500,
			touchEnabled: true,
			swipeThreshold: 50,
			video: false,
			useCSS: true,
			
			// BXSLIDER - PAGER
			pager: true,
			pagerType: 'full',
			pagerShortSeparator: ' / ',
			pagerSelector: null,
			buildPager: null,
			pagerCustom: null,
			
			// BXSLIDER - CONTROLS
			controls: true,
			nextText: 'Next',
			prevText: 'Prev',
			nextSelector: null,
			prevSelector: null,
			autoControls: false,
			startText: 'Start',
			stopText: 'Stop',
			autoControlsCombine: false,
			autoControlsSelector: null,
			
			// BXSLIDER - AUTO
			auto: false,
			pause: 4000,
			autoStart: true,
			autoDirection: 'next',
			autoHover: false,
			autoDelay: 0,
			
			// BXSLIDER - CAROUSEL
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 0,
			slideWidth: 0,
			
			// BXSLIDER - CALLBACKS
			onSliderLoad: function() {},
			onSlideBefore: function() {},
			onSlideAfter: function() {},
			onSlideNext: function() {},
			onSlidePrev: function() {},

			// PRIVATES

			_bxslider: null

        });
    
    },

	validate: function(attrs, options) {

		var _mgbase = this;

		if(_.has(attrs, 'context') && attrs.context)  if(attrs.context.length !== 1) return 'context must have a length of 1';

		if(_.has(attrs, 'viewport')) if(attrs.viewport.length !== 1) return 'viewport must have a length of 1';

		if(_.has(attrs, '_bxslider')) if(attrs.viewport.length !== 1) return '_bxslider must have a length of 1';

	},

	initialize: function() {

		var _mgbase = this;

		_mgbase._config();

		_mgbase.doValidate();

		_mgbase._setBinds();

		_mgbase.attributes.viewport.bxSlider(_mgbase.attributes);

		_mgbase.alert(_mgbase.attributes.setActive);

	},

	_config: function() {

		var _mgbase = this;

		//Viewport of slider
		_mgbase.attributes.viewport = (_mgbase.attributes.viewport) ? $(_mgbase.attributes.viewport) : _mgbase.attributes.sourceEl;

		//Container of viewport and buttons. Dom will be traversed to find controls if built by backend
		_mgbase.attributes.context = (_mgbase.attributes.context) ? _mgbase.attributes.viewport.parents(_mgbase.attributes.context) : null;

		//Lets store a reference to this slider once its initialized

		var _origFn = _mgbase.attributes.onSliderLoad;

		_mgbase.attributes.onSliderLoad = null;

		_mgbase.attributes.onSliderLoad = function() {

			_mgbase.set('_bxslider', _mgbase.attributes.viewport);

			if(typeof _origFn === 'function') _origFn();

		};

	},

	//Call bind methods
	_setBinds: function() {

		var _mgbase = this;

		_mgbase.binds.setActivateEvent(_mgbase);

		_mgbase.binds.setDeactivateEvent(_mgbase);

	},

	//------------------PUBLIC METHODS-------------------

	//Bind methods
	binds: {

		setActivateEvent: function(context) {

			var _mgbase = context;

			//On activate
			_mgbase.on('activate', function(e, data) {

				if(_mgbase.attributes.auto) _mgbase.startAuto();

			});

		},

		setDeactivateEvent: function(context) {

			var _mgbase = context;

			//On deactivate
			_mgbase.on('deactivate', function(e, data) {

				var _attrs = _.clone(_mgbase.attributes);

				_mgbase.reloadSlider(_.extend(_attrs, {
					auto: false,
					startSlide: 0
				}));

			});
			
		}

	},

	onConstructed: function() {

		var _mgbase = this;

	},

	goToSlide: function(slideIndex, direction) { this.attributes._bxslider.goToSlide(slideIndex, direction); },

	goToNextSlide: function() { this.attributes._bxslider.goToNextSlide(); },

	goToPrevSlide: function() { this.attributes._bxslider.goToPrevSlide(); },

	startAuto: function(preventControlUpdate) { this.attributes._bxslider.startAuto(); },

	stopAuto: function(preventControlUpdate) { this.attributes._bxslider.stopAuto(); },

	getCurrentSlide: function() { this.attributes._bxslider.getCurrentSlide(); },

	getSlideCount: function() { this.attributes._bxslider.getSlideCount(); },

	reloadSlider: function(settings) { this.attributes._bxslider.reloadSlider(settings); },

	destroySlider: function() { this.attributes._bxslider.destroySlider(); }

});